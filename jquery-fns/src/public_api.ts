import { createWrapperQuery as $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

export { JQueryFnsWrapper, $create };
