export function isWindow(obj: EventTarget): obj is Window {
  return obj != null && obj === obj['window'];
}

export function isDocument(obj: Node): obj is Document {
  return obj?.nodeType === 9;
}

export function contains(a: Document, b: Node) {
  var adown = isDocument(a) ? a.documentElement : a,
    bup = b && b.parentNode;

  return a === bup || !!(bup && bup.nodeType === 1 && (

    // Support: IE 9 - 11+
    // IE doesn't have `contains` on SVG.
    adown.contains ?
      adown.contains(bup) :
      a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16
  ));
};


export function isAttached(doc: Document, elem: Node) {
  if (!doc.documentElement.getRootNode) {
    return contains(elem.ownerDocument, elem);
  }

  return contains(elem.ownerDocument, elem) ||
    elem.getRootNode({ composed: true }) === elem.ownerDocument;
}

export function nodeName(elem: Element, name: string) {
  return elem?.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
}

export function camelCase(string: string) {
  return string.replace(/-([a-z])/g, (_, letter) => letter.toUpperCase());
}

export function resolveArray<T>(elOrArray: T | T[]): T[] {
  if (Array.isArray(elOrArray)) {
    return elOrArray;
  } else if (elOrArray) {
    return [elOrArray];
  }
  return [];
}

export function mergeObject(first: any, ...args: any[]) {
  const objArgs = args.filter(a => a && typeof a === 'object');
  const [second, ...leftover] = objArgs;
  const firstObj = typeof first === 'object' && first;

  if (!firstObj || !objArgs.length) {
    return objArgs.length && mergeObject(second, ...leftover) || firstObj || {};
  }

  const secondObj = typeof second === 'object' && second;

  return mergeObject(!secondObj ? Object.assign({}, firstObj)
    : Object.assign({}, ...[...Object.keys(firstObj), ...Object.keys(secondObj)]
      .filter((key, idx, ary) => ary.indexOf(key) === idx).map(key => ({
        [key]: ((firstVal, secondVal) => [firstVal, secondVal].every(v => typeof v === 'object' && !Array.isArray(v))
          ? mergeObject(firstVal, secondVal) : typeof secondVal === 'undefined' ? firstVal : secondVal)(firstObj[key], secondObj[key]),
      }))), ...leftover);
}


export function traverseNodes(elem: Node,
  dir: { [K in keyof HTMLElement]: HTMLElement[K] extends Node ? K : never }[keyof HTMLElement],
  cb: (elem: HTMLElement) => { done?: boolean, push?: boolean }) {
  if (!elem) { return []; }

  const stack: HTMLElement[] = [];

  if (elem instanceof HTMLElement) {
    for (let el = elem[dir]; el; el = el[dir]) {
      if (el instanceof HTMLElement) {
        const result = cb(el);
        if (result?.push) { stack.push(el); }
        if (result?.done) { break; }
      }
    }
  }

  return stack;
}
