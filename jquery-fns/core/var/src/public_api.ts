export const pnum = (/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/).source;
export const rcssNum = new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i");
export const rnumnonpx = new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");
export const rcustomProp = /^--/;
export const whitespace = "[\\x20\\t\\r\\n\\f]";
export const rtrim = new RegExp(
  "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$",
  "g"
);
