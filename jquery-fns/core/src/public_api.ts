import { resolveArray, traverseNodes } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { css } from 'ngx-malihu-scrollbar-ex/jquery-fns/css-utils';

export type WrapperSelector = Node[] | Node | string;

export type WrapperExtenderRecord = Record<string, any>;

export interface WrapperExtendsParam<T extends WrapperExtenderRecord, W extends JQueryFnsWrapper = JQueryFnsWrapper> {
  (this: W, params: { document: Document, element: Node, index: number, setupMethod<ST>(fn: () => ST, key?: string): ST }): T
}

export interface WrapperExtends<T extends WrapperExtenderRecord> {
  <W extends JQueryFnsWrapper<T>>(wrapper: W): W;
  (document: Document, elementOrSelector: WrapperSelector): T & JQueryFnsWrapper<T>;
}

export function extendWrapper<T extends WrapperExtenderRecord>(exFn: WrapperExtendsParam<T>): WrapperExtends<T> {
  const extender = (...args: any[]) => {
    const instance: JQueryFnsWrapper = (args.length === 1 && args[0] instanceof JQueryFnsWrapper) && args[0]
      || new JQueryFnsWrapper(args[0], args[1], [extender]);

    const [element, ...leftOverEls] = instance.elements.filter(e => !!e);
    const setupResult = {};
    let setupResultGuid = 0;

    Object.entries(exFn.apply(instance, [{
      document: instance.document, element, index: 0,
      setupMethod(setupFn: () => any, key: string) {
        const result = setupFn();
        (setupResult[setupResultGuid] || (setupResult[setupResultGuid] = {}))[key] = result;
        return result;
      }
    }] as Parameters<WrapperExtendsParam<T>>) || {}).forEach(([key, val]) => {
      if (typeof val === 'function') {
        instance[key] = (...exArgs: any[]) => {
          if (!element) { return; }

          const guid = setupResultGuid;
          const elResult = val.apply(instance, exArgs);
          leftOverEls.forEach((el, idx) => exFn.apply(instance, [{
            document: document, element: el, index: idx + 1,
            setupMethod: (_, key) => setupResult?.[guid]?.[key],
          }] as Parameters<WrapperExtendsParam<T>>)[key].apply(instance, exArgs));
          setupResultGuid++;
          return elResult;
        };
      } else {
        instance[key] = val;
      }
    })

    return instance;
  };

  return extender as any;
}

// export interface SELF<T extends WrapperExtender = WrapperExtender> extends JQueryFnsWrapper<T> { }

export type CreateWrapperQueryReturn<T extends WrapperExtenderRecord> = {
  (selector: JQueryFnsWrapper<T> | WrapperSelector): JQueryFnsWrapper<T> & T,
  // (selector: WrapperSelector): (JQueryFnsWrapper<T> & {
  //   [K in keyof T]: T[K] extends (...args: infer PS) => infer RS
  //   ? ((...args: PS) => (RS extends SELF<T> ? JQueryFnsWrapper<T> : RS)) : T[K];
  // }),
};

export function createWrapperQuery<T extends WrapperExtenderRecord>(doc: Document,
  extender: WrapperExtends<T>): CreateWrapperQueryReturn<T>;
export function createWrapperQuery<T1 extends WrapperExtenderRecord, T2 extends WrapperExtenderRecord>(doc: Document,
  extender1: WrapperExtends<T1>, extender2: WrapperExtends<T2>): CreateWrapperQueryReturn<T1 & T2>;
export function createWrapperQuery<
  T1 extends WrapperExtenderRecord,
  T2 extends WrapperExtenderRecord,
  T3 extends WrapperExtenderRecord,
  >(doc: Document,
    extender1: WrapperExtends<T1>,
    extender2: WrapperExtends<T2>,
    extender3: WrapperExtends<T3>,
): CreateWrapperQueryReturn<T1 & T2 & T3>;
export function createWrapperQuery<
  T1 extends WrapperExtenderRecord,
  T2 extends WrapperExtenderRecord,
  T3 extends WrapperExtenderRecord,
  T4 extends WrapperExtenderRecord,
  >(doc: Document,
    extender1: WrapperExtends<T1>,
    extender2: WrapperExtends<T2>,
    extender3: WrapperExtends<T3>,
    extender4: WrapperExtends<T4>,
): CreateWrapperQueryReturn<T1 & T2 & T3 & T4>;
export function createWrapperQuery<T extends WrapperExtenderRecord>(doc?: Document,
  ...extenders: WrapperExtends<T>[]): CreateWrapperQueryReturn<T>;
export function createWrapperQuery<T extends WrapperExtenderRecord>(doc?: Document,
  ...extenders: WrapperExtends<T>[]): CreateWrapperQueryReturn<T> {
  return selector => {
    const extendedMap = new WeakMap<WrapperExtends<any>, boolean>();
    let inst = selector instanceof JQueryFnsWrapper ? (() => {
      selector.extenders.forEach(ext => extendedMap.set(ext, true));
      return selector;
    })() : new JQueryFnsWrapper(doc || document, selector, extenders);

    extenders.forEach(ext => {
      if (!extendedMap.get(ext)) {
        inst = ext(inst);
        extendedMap.set(ext, true);
      }
    });
    return inst as any;
  }
}

function resolveWrapperSelector(node: ParentNode, elementsOrSelector: WrapperSelector) {
  if (typeof elementsOrSelector === 'string') {
    const els: Node[] = [];

    if (node instanceof Document && elementsOrSelector.startsWith('<') && elementsOrSelector.endsWith('>') && elementsOrSelector.length >= 3) {
      const div = node.createElement('div');
      div.innerHTML = elementsOrSelector;
      div.childNodes.forEach(child => {
        if (child instanceof Node) {
          els.push(child);
        }
      })
    } else {
      // FIXME: will apply pseudos select later
      let isFirst: boolean;
      const ps = elementsOrSelector.split(':').filter(sel => {
        if (sel === 'first') {
          isFirst = true;
          return false;
        }
        return true;
      }).join(':');

      node.querySelectorAll(ps).forEach((el, idx) => {
        if (isFirst !== undefined && !isFirst && idx > 0) {
          return;
        }

        if (el instanceof Node) {
          els.push(el);
        }
      });
    }
    return els;
  }

  return resolveArray(elementsOrSelector).filter(el => el as Node) as Node[];
}

export interface JQueryFnsWrapper {
  [ord: number]: HTMLElement;
}

export class JQueryFnsWrapper<EXT extends WrapperExtenderRecord = {}> {
  readonly document: Document;
  readonly elements: Node[];

  get element() { return this.elements[0]; }
  get length() { return this.elements.length; }

  constructor(
    doc: Document,
    readonly selector: WrapperSelector,
    readonly extenders: WrapperExtends<EXT>[] = [],
  ) {
    this.document = doc || document;
    this.elements = resolveWrapperSelector(this.document, selector);
    this.elements.forEach((el, idx) => Object.defineProperty(this, idx, {
      enumerable: true,
      get: () => el instanceof HTMLElement ? el : undefined,
    }));
  }

  next(selector?: string) {
    return this.selectNew(traverseNodes(this.element, 'nextSibling', el => (!selector || el.matches(selector)) && { push: true, done: true })[0]);
  }

  children(selector?: string) {
    const children: Node[] = [];

    this.elements.forEach(el => {
      el.childNodes.forEach(child => {
        if (typeof selector !== 'string' || (child instanceof Element && child.matches(selector))) {
          children.push(child);
        }
      });
    });

    return this.selectNew(children);
  }

  contents() {
    const contents: Node[] = []

    this.elements.forEach(el => {
      el.childNodes.forEach(child => contents.push(child));
    });

    return contents;
  }

  parent(selector?: string) {
    let p = this.elements[0]?.parentElement;

    if (p && selector && !p.matches(selector)) {
      p = null;
    }

    return this.selectNew(p);
  }

  parents(selector?: string) {
    const parents: Node[] = traverseNodes(this.element, 'parentElement', el =>
      (false === (selector ?? false) || el.matches(selector)) && { push: true });

    return this.selectNew(parents);
  }

  parentsUntil(selector: string | Node, filter?: string) {
    const parents: Node[] = traverseNodes(this.element, 'parentElement', el =>
      (selector instanceof Node ? selector === el : el.matches(selector))
        ? { done: true } : { push: filter === undefined || el.matches(filter) });

    return this.selectNew(parents);
  }

  closest(selector: string) {
    const $w = createWrapperQuery<EXT>(this.document, ...this.extenders);

    return $w(traverseNodes(this.element, 'parentElement', el =>
      el.matches(selector) && { push: true, done: true })[0]);
  }

  offsetParent() {
    const el = (() => {
      const [element] = this.elements;

      if (element instanceof HTMLElement) {
        let offsetParent: HTMLElement = element.offsetParent as any;

        while (offsetParent && css(this.document, offsetParent, "position") === "static") {
          offsetParent = offsetParent.offsetParent as any;
        }

        return offsetParent
      }
    })() || this.document.documentElement;

    return createWrapperQuery<EXT>(this.document, ...this.extenders)(el);
  }

  eq(index: number) {
    return this.selectNew(this.elements[index]);
  }

  find(selector: WrapperSelector) {
    return createWrapperQuery<EXT>(this.document, ...this.extenders)(
      [].concat(...this.elements.map(el => resolveWrapperSelector((el instanceof Element ? el : this.document), selector))),
    );
  }

  each(cb: (this: HTMLElement) => void) {
    this.elements.forEach(el => {
      if (el instanceof HTMLElement) {
        cb?.apply(el);
      }
    });
    return this;
  }

  map<T>(cb: (this: Node, index: number, domElement: Node) => T): T[] {
    return this.elements.map((el, idx) => cb?.call(el, idx, el));
  }

  extendUtils<T extends WrapperExtenderRecord>(extender: WrapperExtends<T>): JQueryFnsWrapper<EXT & T> & EXT & T
  extendUtils<
    T1 extends WrapperExtenderRecord,
    T2 extends WrapperExtenderRecord,
    >(
      extender1: WrapperExtends<T1>,
      extender2: WrapperExtends<T2>,
  ): JQueryFnsWrapper<EXT & T1 & T2> & EXT & T1 & T2
  extendUtils<
    T1 extends WrapperExtenderRecord,
    T2 extends WrapperExtenderRecord,
    T3 extends WrapperExtenderRecord,
    >(
      extender1: WrapperExtends<T1>,
      extender2: WrapperExtends<T2>,
      extender3: WrapperExtends<T3>,
  ): JQueryFnsWrapper<EXT & T1 & T2 & T3> & EXT & T1 & T2 & T3
  extendUtils<
    T1 extends WrapperExtenderRecord,
    T2 extends WrapperExtenderRecord,
    T3 extends WrapperExtenderRecord,
    T4 extends WrapperExtenderRecord,
    >(
      extender1: WrapperExtends<T1>,
      extender2: WrapperExtends<T2>,
      extender3: WrapperExtends<T3>,
      extender4: WrapperExtends<T4>,
  ): JQueryFnsWrapper<EXT & T1 & T2 & T3 & T4> & EXT & T1 & T2 & T3 & T4
  extendUtils<T extends WrapperExtenderRecord, TS extends WrapperExtenderRecord>(
    extender: WrapperExtends<T>, ...extenders: WrapperExtends<TS>[]): JQueryFnsWrapper<EXT & T & TS> & EXT & T & TS
  extendUtils(...extenders: WrapperExtends<any>[]) {
    const validExtenders = extenders.filter(ext => ext && !this.extenders.some(e => e === ext));

    if (!validExtenders.length) {
      return this
    }

    return createWrapperQuery(this.document, ...this.extenders, ...validExtenders)(this.elements) as any;
  }

  selectNew(elementsOrSelector: WrapperSelector, doc?: Document) {
    return createWrapperQuery<EXT>(doc ?? this.document, ...this.extenders)(elementsOrSelector);
  }

  selectDocument() {
    return createWrapperQuery<EXT>(this.document, ...this.extenders)(this.document as any);
  }

  clone(deep?: boolean) {
    return this.selectNew(this.map((_, el) => el.cloneNode(deep) as Node));
  }

  add(elementsOrSelector: WrapperSelector | JQueryFnsWrapper) {
    return createWrapperQuery<EXT>(this.document, ...this.extenders)([
      ...this.elements,
      ...(elementsOrSelector instanceof JQueryFnsWrapper ? elementsOrSelector.elements
        : resolveWrapperSelector(this.document, elementsOrSelector)),
    ]);
  }

  append(content: WrapperSelector | JQueryFnsWrapper) {
    const $content = content instanceof JQueryFnsWrapper ? content : this.selectNew(content);

    this.elements.forEach(parent => $content.elements.forEach(el => parent.appendChild(el)));

    return this;
  }

  prepend(content: WrapperSelector | JQueryFnsWrapper) {
    const $content = content instanceof JQueryFnsWrapper ? content : this.selectNew(content);

    this.elements.forEach(parent => {
      const firstChild = parent.firstChild;
      $content.elements.forEach(el => parent.insertBefore(el, firstChild));
    });

    return this;
  }

  before(content: WrapperSelector | JQueryFnsWrapper) {
    const $content = content instanceof JQueryFnsWrapper ? content : this.selectNew(content);

    this.elements.forEach(el => {
      if (el.parentNode) {
        $content.elements.forEach(contentEl => el.parentNode.insertBefore(contentEl, el));
      }
    });
  }

  after(content: WrapperSelector | JQueryFnsWrapper) {
    const $content = content instanceof JQueryFnsWrapper ? content : this.selectNew(content);

    this.elements.forEach(el => {
      if (el.parentNode) {
        $content.elements.forEach(contentEl => el.parentNode.insertBefore(contentEl, el.nextSibling));
      }
    });
  }

  wrap(html: string) {
    const $this = this;
    this.each(function () {
      $this.selectNew(this).wrapAll(html);
    });
  }

  wrapAll(html: string) {
    if (this[0]) {
      const wrap = this.selectNew(html, this[0].ownerDocument).eq(0).clone(true);

      if (this[0].parentNode) {
        this.eq(0).before(wrap);
      }

      this.selectNew(wrap.map(function () {
        let elem = this;

        while (elem['firstElementChild'] instanceof Node) {
          elem = elem['firstElementChild'];
        }

        return elem;
      })).eq(0).append(this);
    }
  }

  unwrap(selector?: string) {
    const $this = this;

    this.parent(selector).each(function () {
      if (!this.matches('body')) {
        const els: Node[] = [];
        this.childNodes.forEach(node => {
          if (node instanceof Node) {
            els.push(node);
          }
        });

        $this.selectNew(this).replaceWith(els);
      }
    })
  }

  replaceWith(content: WrapperSelector | JQueryFnsWrapper) {
    const $content = content instanceof JQueryFnsWrapper ? content : this.selectNew(content);

    if (!$content.length) { return; }

    this.each(function () {
      const { parentElement } = this;

      if (parentElement) {
        parentElement.replaceChild($content.element, this);
        const nextSib = $content.element.nextSibling;

        for (let i = 1; i < $content.length; i++) {
          if (nextSib) {
            parentElement.insertBefore($content[i], nextSib);
          } else {
            parentElement.append($content[i]);
          }
        }
      }
    })
  }

  remove() {
    while (this.elements.length) {
      const elem = this.elements.shift();
      elem?.parentNode?.removeChild(elem);
    }
    return this;
  }
}

