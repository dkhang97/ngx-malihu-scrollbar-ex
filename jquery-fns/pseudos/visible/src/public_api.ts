import { createPseudoRegistry } from "ngx-malihu-scrollbar-ex/jquery-fns/extenders/pseudo";

function isVisible(elem: HTMLElement) {
  return !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length);
}

export const $pseudo = createPseudoRegistry({
  visible: ({ element }) => isVisible(element),
  hidden: ({ element }) => !isVisible(element),
});
