import { extendWrapper, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';
import { resolveArray } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';

import type { WrapperExtends } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

export type WrappedEvent<E extends Event> = E & WrapperEvent<E>;

type EventCallback<E extends Event> = (this: HTMLElement, event: WrappedEvent<E>, ...extra: any[]) => void;
type EventArgsConverter = <E extends Event>(this: JQueryFnsWrapper, event: E) => Event | WrapperEvent<E> | [Event | WrapperEvent<E>, ...any[]];

type SourceTypeSingle = { type: string, passive?: boolean };
type SourceTypeLit = string | SourceTypeSingle | (string | SourceTypeSingle)[];
type SourceTypes = SourceTypeLit | ((wrapper: JQueryFnsWrapper) => SourceTypeLit);

function normalizeSourceTypes(sourceTypes: SourceTypes, wrapper: JQueryFnsWrapper): SourceTypeSingle[] {
  const lit = typeof sourceTypes === "function" ? sourceTypes(wrapper) : sourceTypes;
  const sources = resolveArray(lit);

  return sources.filter(s => !!s).map(type => typeof type === 'string' ? { type } : type);
}

interface EventCacheRecord {
  event: string;
  namespaces: string[];
  passive?: boolean;
  handler(ev: Event): void;
}

const boundEvents = new WeakMap<Node, EventCacheRecord[]>();

const forwardedEvents: Record<string, {
  sourceTypes: SourceTypes,
  converter: EventArgsConverter,
  setup: (wrapper: JQueryFnsWrapper) => void,
  passive: boolean,
}[]> = {};

export interface EventForwardExtra {
  converter?: EventArgsConverter;
  setup?(wrapper: JQueryFnsWrapper): void;
  passive?: boolean
}

export function createEventRegistry(sourceTypes: SourceTypes, targetType: string, extra?: EventForwardExtra) {
  let registered = false;

  const register = () => {
    if (registered) { return; }

    if (sourceTypes && targetType) {
      const fw = forwardedEvents[targetType] || (forwardedEvents[targetType] = []);

      // (Array.isArray(sourceTypes) ? sourceTypes : [sourceTypes]).forEach(sourceType => {
      fw.push({ sourceTypes, converter: extra?.converter, setup: extra?.setup, passive: extra?.passive ?? false });
      // })
    }

    registered = true;
  };

  return { register };
}

function resolveEventQuery(eventQuery: string, includeEmptyType?: boolean) {
  return (eventQuery || '').split(/\s+/)
    .map(t => t?.split('.').map(n => n.trim()))
    .filter(t => includeEmptyType || !!t[0])
    .map(([type, ...namespaces]) => ({ type, namespaces }));
}


export const $extender = extendWrapper(function ({ element, setupMethod }) {
  const inst = this;
  return {
    // namespace currently not supported
    bind<E extends Event>(eventQuery: string, handler: EventCallback<E>) {
      const evs = setupMethod(() => {
        const events: EventCacheRecord[] = [];

        resolveEventQuery(eventQuery).forEach(({ type: evType, namespaces }) => {
          const fw = forwardedEvents[evType];

          if (fw?.length) {
            fw.forEach(({ sourceTypes, converter, setup }) => {
              setup(inst);

              normalizeSourceTypes(sourceTypes, inst).forEach(event => {
                events.push({
                  event: event.type, passive: event.passive, namespaces, handler: ev => {
                    const convertResult = typeof converter === 'function' ? converter?.call(inst, ev) : ev;
                    const [convertedEvent, ...extra] = resolveArray(convertResult);
                    handler?.call(element, convertedEvent ? WrapperEvent.create(convertedEvent)
                      : WrapperEvent.create(ev), ...extra);
                  },
                });
              });

            });
          } else {
            events.push({ event: evType, namespaces, handler: ev => handler?.call(element, WrapperEvent.create(ev)) });
          }
        });

        return events;
      });

      evs.forEach(ev => {
        const { event, passive } = ev;
        const optsWithPassive: AddEventListenerOptions = (passive ?? event.startsWith('touch')) ? { passive: true } : undefined;
        if (optsWithPassive) {
          const orgHandler = ev.handler;
          ev.handler = evArg => {
            evArg.preventDefault = () => { };
            orgHandler.call(element, evArg);
          };
        }

        element.addEventListener(event, ev.handler, optsWithPassive);
      });

      let boundEv = boundEvents.get(element);
      if (!boundEv) { boundEvents.set(element, boundEv = []); }
      boundEv.push(...evs);

      return evs;
    },
    unbind(eventQuery: string) {
      const elEvs = boundEvents.get(element);
      if (elEvs?.length) {
        const evQS = resolveEventQuery(eventQuery, true);
        boundEvents.set(element, elEvs.filter(ev => {
          return !evQS.some(evQ => {
            const namespaceMatch = () => evQ.namespaces.some(ns => ~ev.namespaces.indexOf(ns));
            // same event
            return (evQ.type === '' || evQ.type === ev.event)
              && (evQ.type !== '' && (!evQ.namespaces.length || namespaceMatch()))
              && (evQ.type === '' && namespaceMatch());
          })
        }));
      }
    }
  };
});

export class WrapperEvent<E extends Event> {
  static overrideFields = ['type', 'target', 'currentTarget', 'timeStamp'];

  static create<E extends Event>(originalEvent: E, type?: string): Record<string, any> & WrappedEvent<E> {
    if (originalEvent.constructor === WrapperEvent) {
      return originalEvent;
    }

    const wpEv = new WrapperEvent(originalEvent);

    if (type !== undefined) {
      wpEv.type = type;
    }

    const ovEntries = Object.entries(wpEv).map(([key, val]) => {
      if (WrapperEvent.overrideFields.includes(key)) {
        delete wpEv[key];
        return { key, val };
      }
    }).filter(o => !!o);

    const ev = Object.assign(new Event(type ?? originalEvent.type), wpEv);
    ev.constructor = WrapperEvent;
    ev['__proto__'] = WrapperEvent;
    // const ev = new WrapperEvent(originalEvent);

    ovEntries.forEach(({ key, val }) => Object.defineProperty(ev, key, { get: () => val }));

    return ev as any;
  }

  type = this.originalEvent.type;
  isDefaultPrevented = this.originalEvent.defaultPrevented ? (() => true) : (() => false);
  target = this.originalEvent.target;
  currentTarget = this.originalEvent.currentTarget;
  relatedTarget: EventTarget = this.originalEvent['relatedTarget'];
  timeStamp = this.originalEvent.timeStamp || Date.now();
  isSimulated = false;

  constructor(
    readonly originalEvent: E,
  ) { }

  isPropagationStopped = () => false;

  isImmediatePropagationStopped = () => false;

  preventDefault = () => {
    const e = this.originalEvent;

    this.isDefaultPrevented = () => true;

    if (e && !this.isSimulated) {
      e.preventDefault();
    }
  }

  stopPropagation = () => {
    const e = this.originalEvent;

    this.isPropagationStopped = () => true;

    if (e && !this.isSimulated) {
      e.stopPropagation();
    }
  }

  stopImmediatePropagation = () => {
    const e = this.originalEvent;

    this.isImmediatePropagationStopped = () => true;

    if (e && !this.isSimulated) {
      e.stopImmediatePropagation();
    }

    this.stopPropagation();
  }
}
