import { extendWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';
import { resolveArray } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { css, style } from 'ngx-malihu-scrollbar-ex/jquery-fns/css-utils';

import type { WrapperExtends } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

function resolveClasses(element: Element) {
  return (element instanceof Element) && element.getAttribute('class')?.split(' ').map(cls => cls.trim()) || [];
}

function normalizeClasses(classes: string[]) {
  return [].concat(...classes.map(cls => cls.split(' ')))
    .map(cls => cls?.trim()).filter((cls, idx, ary) => cls && ary.indexOf(cls) === idx);
}


const _addClass = (element: Node) => (className: string | string[]) => {
  if (!(element instanceof Element)) { return; }

  const classes = normalizeClasses(resolveArray(className));
  if ('classList' in element) {
    element.classList.add(...classes);
  } else {
    classes.push(...resolveClasses(element));
    (element as Element).setAttribute('class', classes.join(' '));
  }
};

const _removeClass = (element: Node) => (className: string | string[]) => {
  if (!(element instanceof Element)) { return; }

  let classes = normalizeClasses(resolveArray(className));
  if ('classList' in element) {
    element.classList.remove(...classes);
  } else {
    const clsToRemove = resolveClasses(element);
    classes = classes.filter(cls => !(~(clsToRemove?.indexOf(cls) ?? -1)));
    (element as Element).setAttribute('class', classes.join(' '));
  }
};

const _hasClass = (element: Node) => (className: string | string[]) => {
  if (!(element instanceof Element)) { return false; }

  const classes = normalizeClasses(resolveArray(className));
  if ('classList' in element) {
    return classes.some(cls => element.classList.contains(cls));
  } else {
    return resolveClasses(element).some(cls => ~(classes?.indexOf(cls) ?? -1));
  }
};

export const $extender = extendWrapper(function ({ document, element }) {
  return {
    css: (function (nameOrObj, value) {
      if (arguments.length === 1) {
        if (typeof nameOrObj === "string") {
          return css(document, element, nameOrObj);
        } else {
          Object.entries(nameOrObj).forEach(([key, val]) => {
            style(document, element, key, val);
          });
        }
      } else if (arguments.length > 1) {
        style(document, element, nameOrObj, value);
      }

      return this;
    }) as {
      (name: string): any,
      (name: string, value: any): void,
      (object: Record<string, any>): void,
    },
    hasClass: _hasClass(element),
    addClass: _addClass(element),
    removeClass: _removeClass(element),
    toggleClass(className: string | string[], stateVal?: boolean) {
      const addClass = _addClass(element);
      const removeClass = _removeClass(element);
      const hasClass = _hasClass(element);

      if (typeof stateVal === 'boolean') {
        stateVal ? addClass(className) : removeClass(className);
      } else {
        resolveArray(className).forEach(cls =>
          hasClass(cls) ? removeClass(cls) : addClass(cls));
      }
    }
  };
});
