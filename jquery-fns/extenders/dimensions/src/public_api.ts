import { extendWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';
import { isDocument, isWindow } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { css } from 'ngx-malihu-scrollbar-ex/jquery-fns/css-utils';

import type { WrapperExtends } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

export type DimensionRecord = Record<'width' | 'height' | 'innerWidth' | 'innerHeight' | 'outerWidth' | 'outerHeight', (margin?: boolean) => number>;

export const $extender = extendWrapper<DimensionRecord>(({ document, element: elem }) => {
  const dim = Object.entries({ Height: "height", Width: "width" }).map(([name, type]) => {
    const dimZone = Object.entries({
      padding: "inner" + name,
      content: type,
      "": "outer" + name
    }).map(([defaultExtra, funcName]) => ({
      [funcName]: function (margin: boolean) {
        const extra = defaultExtra || (margin === true ? "margin" : "border");

        if (isWindow(elem)) {
          return funcName.indexOf("outer") === 0 ?
            elem["inner" + name] :
            elem.document.documentElement["client" + name];
        }

        if (isDocument(elem)) {
          const doc = elem.documentElement;

          // Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
          // whichever is greatest
          return Math.max(
            elem['body']["scroll" + name], doc["scroll" + name],
            elem['body']["offset" + name], doc["offset" + name],
            doc["client" + name]
          );
        }

        return css(document, elem, type, extra);
      }
    }));

    return Object.assign({}, ...dimZone);
  });

  return Object.assign({}, ...dim);
});
