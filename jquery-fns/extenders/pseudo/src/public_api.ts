import { extendWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

import type { WrapperExtends } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

export type PseudoChecker = (params: { document: Document, element: HTMLElement, index: number }) => boolean;

const registeredPseudos: Record<string, PseudoChecker> = {};

export function createPseudoRegistry(pseudoMap: Record<string, PseudoChecker>) {
  let registered = false;

  const register = () => {
    if (registered) { return; }

    Object.entries(pseudoMap)
      .forEach(([entry, checker]) => registeredPseudos[entry] = checker);

    registered = true;
  };

  return { register };
}


export const $extender = extendWrapper(({ document, element, index }) => ({
  is(selector: string) {
    if (!(element instanceof Element)) { return false; }

    const customCheckers: PseudoChecker[] = [];

    const finalSelector = selector?.split(':').filter((part, idx) => {
      if (idx > 0) {
        const pseudo = registeredPseudos[part];

        if (pseudo) {
          customCheckers.push(pseudo);
          return false;
        }
      }

      return true;
    }).join(':');

    if (!customCheckers.length) {
      return finalSelector ? element.matches(finalSelector) : false;
    }

    const pseudoMatched = element instanceof HTMLElement &&
      customCheckers.every(checker => checker({ document, element: element as HTMLElement, index }));

    if (pseudoMatched) {
      return !finalSelector || element.matches(finalSelector);
    } else {
      return finalSelector ? element.matches(finalSelector) : false;
    }
  }
}));
