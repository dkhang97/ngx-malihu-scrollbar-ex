import { extendWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';
import { isDocument, isWindow } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';

import type { WrapperExtends } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

function resolveScroll(elem: Node, method: string, top: boolean) {
  return ((val?: any) => {
    let win: Window;
    if (isWindow(elem)) {
      win = elem;
    } else if (isDocument(elem)) {
      win = elem.defaultView;
    }

    if (val === undefined) {
      return win ? (top ? (win.scrollY ?? win.pageYOffset) : (win.scrollX ?? win.pageXOffset)) : elem[method];
    }

    if (win) {
      win.scrollTo(
        !top ? val : (win.scrollX ?? win.pageXOffset),
        top ? val : (win.scrollY ?? win.pageYOffset),
      );

    } else {
      elem[method] = val;
    }
  }) as {
    (): any,
    (val: any): void,
  }
}

export const $extender = extendWrapper(({ element }) => ({
  offset() {
    if (!(element instanceof Element)) {
      return;
    }

    // Return zeros for disconnected and hidden (display: none) elements (gh-2310)
    // Support: IE <=11+
    // Running getBoundingClientRect on a
    // disconnected node in IE throws an error
    if (!element.getClientRects().length) {
      return { top: 0, left: 0 };
    }

    // Get document-relative position by adding viewport scroll to viewport-relative gBCR
    const rect = element.getBoundingClientRect();
    const win = element.ownerDocument.defaultView;
    return {
      top: rect.top + win.pageYOffset,
      left: rect.left + win.pageXOffset
    };
  },
  scrollLeft: resolveScroll(element, 'scrollLeft', false),
  scrollTop: resolveScroll(element, 'scrollTop', false),
}));
