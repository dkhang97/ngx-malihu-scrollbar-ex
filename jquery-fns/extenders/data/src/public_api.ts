import { extendWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

import type { WrapperExtends } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';

const elDataMap = new WeakMap<Node, {}>();

function retrieveData(element: Node) {
  let elData = elDataMap.get(element);
  if (!elData) {
    elDataMap.set(element, elData = {});
  }
  return elData;
}

export const $extender = extendWrapper(({ element }) => ({
  data: (function (key, value) {
    if (arguments.length === 1) {
      if (key && typeof key === "object") {
        const elData = retrieveData(element);
        Object.entries(key).forEach(([k, v]) => elData[k] = v);
        return;
      }

      return elDataMap.get(element)?.[key] ?? (element as HTMLElement)?.dataset?.[key];
    } else if (arguments.length > 1) {
      retrieveData(element)[key] = value;
    }
  }) as {
    (key: string | number): any,
    (key: string | number, value: any): void,
    (obj: Record<string | number, any>): void,
  },
  removeData(key: string) {
    const elData = elDataMap.get(element);

    if (elData) {
      delete elData[key];
    }

    const dSet = (element as HTMLElement)?.dataset;

    if (dSet) {
      delete dSet[key];
    }
  }
}));
