import { $extender as $css } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/css';
import { $extender as $data } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/data';
import { $extender as $dimensions } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/dimensions';
import { $extender as $event } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/event';
import { $extender as $offset } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/offset';
import { $extender as $pseudo } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/pseudo';

export { $css, $data, $dimensions, $event, $offset, $pseudo };
