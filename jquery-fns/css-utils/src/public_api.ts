import proto from './lib/proto';
const { css, style } = proto;
export { css, style };

import './lib/css';
import './lib/style';
import './lib/adjustCSS';
import './lib/curCSS';
