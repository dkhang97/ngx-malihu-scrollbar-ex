export default {
  css(doc: Document, elem: Node, name: string, extra?: string | boolean, styles?: CSSStyleDeclaration): any { },
  style(doc: Document, elem: Node, name: string, value?: any, extra?: string | boolean): any { },
  curCSS(doc: Document, elem: Node, name: string, computed?: CSSStyleDeclaration): any { },
  adjustCSS(doc: Document, elem: Node, prop: string, valueParts): any { },
}
