import { isAttached } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { rcustomProp, rtrim } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/var';

import proto from './proto';
import { getStyles } from './shared';


proto.curCSS = (doc, elem, name, computed) => {
  var ret,
    isCustomProp = rcustomProp.test(name);

  computed = computed || getStyles(elem);

  // getPropertyValue is needed for `.css('--customProperty')` (gh-3144)
  if (computed) {
    ret = computed.getPropertyValue(name) || computed[name];

    // trim whitespace for custom property (issue gh-4926)
    if (isCustomProp) {
      ret = ret.replace(rtrim, "$1");
    }

    if (ret === "" && !isAttached(doc, elem)) {
      ret = proto.style(doc, elem, name);
    }
  }

  return ret !== undefined ?

    // Support: IE <=9 - 11+
    // IE returns zIndex value as an integer.
    ret + "" :
    ret;
}

