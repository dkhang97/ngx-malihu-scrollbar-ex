import { nodeName } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { rcssNum, rnumnonpx } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/var';

import proto from './proto';
import { cssCamelCase, cssExpand, cssHooks, getStyles, isIECheck, setPositiveNumber, swap } from './shared';
import resolveCssSupport from './support';

var

  // Swappable if display is none or starts with table
  // except "table", "table-cell", or "table-caption"
  // See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
  rdisplayswap = /^(none|table(?!-c[ea]).+)/,
  cssShow = { position: "absolute", visibility: "hidden", display: "block" },
  cssNormalTransform = {
    letterSpacing: "0",
    fontWeight: "400"
  };


function boxModelAdjustment(doc: Document, elem: Element, dimension: string, box: string, isBorderBox: boolean, styles: CSSStyleDeclaration, computedVal?: number) {
  var i = dimension === "width" ? 1 : 0,
    extra = 0,
    delta = 0;

  // Adjustment may not be necessary
  if (box === (isBorderBox ? "border" : "content")) {
    return 0;
  }

  for (; i < 4; i += 2) {

    // Both box models exclude margin
    if (box === "margin") {
      delta += proto.css(doc, elem, box + cssExpand[i], true, styles);
    }

    // If we get here with a content-box, we're seeking "padding" or "border" or "margin"
    if (!isBorderBox) {

      // Add padding
      delta += proto.css(doc, elem, "padding" + cssExpand[i], true, styles);

      // For "border" or "margin", add border
      if (box !== "padding") {
        delta += proto.css(doc, elem, "border" + cssExpand[i] + "Width", true, styles);

        // But still keep track of it otherwise
      } else {
        extra += proto.css(doc, elem, "border" + cssExpand[i] + "Width", true, styles);
      }

      // If we get here with a border-box (content + padding + border), we're seeking "content" or
      // "padding" or "margin"
    } else {

      // For "content", subtract padding
      if (box === "content") {
        delta -= proto.css(doc, elem, "padding" + cssExpand[i], true, styles);
      }

      // For "content" or "padding", subtract border
      if (box !== "margin") {
        delta -= proto.css(doc, elem, "border" + cssExpand[i] + "Width", true, styles);
      }
    }
  }

  // Account for positive content-box scroll gutter when requested by providing computedVal
  if (!isBorderBox && computedVal >= 0) {

    // offsetWidth/offsetHeight is a rounded sum of content, padding, scroll gutter, and border
    // Assuming integer scroll gutter, subtract the rest and round down
    delta += Math.max(0, Math.ceil(
      elem["offset" + dimension[0].toUpperCase() + dimension.slice(1)] -
      computedVal -
      delta -
      extra -
      0.5

      // If offsetWidth/offsetHeight is unknown, then we can't determine content-box scroll gutter
      // Use an explicit zero to avoid NaN (gh-3964)
    )) || 0;
  }

  return delta;
}

function getWidthOrHeight(doc: Document, elem: Element, dimension: string, extra: string | number) {
  const isIE = isIECheck(doc);
  // Start with computed style
  var styles = getStyles(elem),

    // To avoid forcing a reflow, only fetch boxSizing if we need it (gh-4322).
    // Fake content-box until we know it's needed to know the true value.
    boxSizingNeeded = isIE || extra,
    isBorderBox = boxSizingNeeded &&
      proto.css(doc, elem, "boxSizing", false, styles) === "border-box",
    valueIsBorderBox = isBorderBox,

    val = proto.curCSS(doc, elem, dimension, styles),
    offsetProp = "offset" + dimension[0].toUpperCase() + dimension.slice(1);

  // Return a confounding non-pixel value or feign ignorance, as appropriate.
  if (rnumnonpx.test(val)) {
    if (!extra) {
      return val;
    }
    val = "auto";
  }


  if ((

    // Fall back to offsetWidth/offsetHeight when value is "auto"
    // This happens for inline elements with no explicit setting (gh-3571)
    val === "auto" ||

    // Support: IE 9 - 11+
    // Use offsetWidth/offsetHeight for when box sizing is unreliable.
    // In those cases, the computed value can be trusted to be border-box.
    (isIE && isBorderBox) ||

    // Support: IE 10 - 11+
    // IE misreports `getComputedStyle` of table rows with width/height
    // set in CSS while `offset*` properties report correct values.
    // Support: Firefox 70+
    // Firefox includes border widths
    // in computed dimensions for table rows. (gh-4529)
    (!resolveCssSupport(doc)?.reliableTrDimensions() && nodeName(elem, "tr"))) &&

    // Make sure the element is visible & connected
    elem.getClientRects().length) {

    isBorderBox = proto.css(doc, elem, "boxSizing", false, styles) === "border-box";

    // Where available, offsetWidth/offsetHeight approximate border box dimensions.
    // Where not available (e.g., SVG), assume unreliable box-sizing and interpret the
    // retrieved value as a content box dimension.
    valueIsBorderBox = offsetProp in elem;
    if (valueIsBorderBox) {
      val = elem[offsetProp];
    }
  }

  // Normalize "" and auto
  val = parseFloat(val) || 0;

  // Adjust for the element's box model
  return (val +
    boxModelAdjustment(
      doc,
      elem,
      dimension,
      extra as string || (isBorderBox ? "border" : "content"),
      valueIsBorderBox,
      styles,

      // Provide the current computed size to request scroll gutter calculation (gh-3589)
      val
    )
  ) + "px";
}

["height", "width"].forEach(dimension => cssHooks[dimension] = {
  get(doc, elem: HTMLElement, computed, extra) {
    if (computed) {
      // Certain elements can have dimension info if we invisibly show them
      // but it must have a current display style that would benefit
      return rdisplayswap.test(proto.css(doc, elem, "display")) &&

        // Support: Safari <=8 - 12+, Chrome <=73+
        // Table columns in WebKit/Blink have non-zero offsetWidth & zero
        // getBoundingClientRect().width unless display is changed.
        // Support: IE <=11+
        // Running getBoundingClientRect on a disconnected node
        // in IE throws an error.
        (!elem.getClientRects().length || !elem.getBoundingClientRect().width) ?
        swap(elem, cssShow, function () {
          return getWidthOrHeight(doc, elem, dimension, extra as string);
        }) :
        getWidthOrHeight(doc, elem, dimension, extra as string);
    }
  },
  set(doc, elem: HTMLElement, value, extra) {
    var matches,
      styles = getStyles(elem),

      // To avoid forcing a reflow, only fetch boxSizing if we need it (gh-3991)
      isBorderBox = extra &&
        proto.css(doc, elem, "boxSizing", false, styles) === "border-box",
      subtract = extra ?
        boxModelAdjustment(
          doc,
          elem,
          dimension,
          extra as string,
          isBorderBox,
          styles
        ) : 0;

    // Convert to pixels if value adjustment is needed
    if (subtract && (matches = rcssNum.exec(value)) &&
      (matches[3] || "px") !== "px") {

      elem.style[dimension] = value;
      value = proto.css(doc, elem, dimension);
    }

    return setPositiveNumber(elem, value, subtract);
  }
});

proto.css = (doc, elem, name, extra, styles) => {
  var val, num, hooks,
    origName = cssCamelCase(name);


  // Try prefixed name followed by the unprefixed name
  hooks = cssHooks[name] || cssHooks[origName];

  // If a hook was provided get the computed value from there
  if (hooks && "get" in hooks) {
    val = hooks.get(doc, elem, true, extra);
  }

  // Otherwise, if a way to get the computed value exists, use that
  if (val === undefined) {
    val = proto.curCSS(doc, elem, name, styles);
  }

  // Convert "normal" to computed value
  if (val === "normal" && name in cssNormalTransform) {
    val = cssNormalTransform[name];
  }

  // Make numeric if forced or a qualifier was provided and val looks numeric
  if (extra === "" || extra) {
    num = parseFloat(val);
    return extra === true || isFinite(num) ? num || 0 : val;
  }

  return val;
}
