import { camelCase } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { rcssNum } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/var';

export const cssExpand = ["Top", "Right", "Bottom", "Left"];

export function getStyles(elem: Node) {
  if (!(elem instanceof Element)) { return; }

  // Support: IE <=11+ (trac-14150)
  // In IE popup's `window` is the opener window which makes `window.getComputedStyle( elem )`
  // break. Using `elem.ownerDocument.defaultView` avoids the issue.
  var view = elem.ownerDocument.defaultView;

  // `document.implementation.createHTMLDocument( "" )` has a `null` `defaultView`
  // property; check `defaultView` truthiness to fallback to window in such a case.
  if (!view) {
    view = window;
  }

  return view.getComputedStyle(elem);
}


export function cssCamelCase(string: string) {
  return camelCase(string.replace(/^-ms-/, "ms-"));
}

export const isIECheck = (doc: Document = document) => doc?.['documentMode'];

export function isAutoPx(prop: string) {
  return /^[a-z]/.test(prop) &&
    /^(?:Border(?:Top|Right|Bottom|Left)?(?:Width|)|(?:Margin|Padding)?(?:Top|Right|Bottom|Left)?|(?:Min|Max)?(?:Width|Height))$/.test(prop[0].toUpperCase() + prop.slice(1));
}

export const cssHooks: Record<string, {
  get?(doc: Document, element: Node, computed?: boolean, extra?: string | boolean): any,
  set?(doc: Document, element: Node, value: any, extra?: string | boolean): void,
}> = {};

export function swap(elem: HTMLElement, options: {}, callback: (this: HTMLElement) => void) {
  var ret, name,
    old = {};

  // Remember the old values, and insert the new ones
  for (name in options) {
    old[name] = elem.style[name];
    elem.style[name] = options[name];
  }

  ret = callback.call(elem);

  // Revert the old values
  for (name in options) {
    elem.style[name] = old[name];
  }

  return ret;
}


export function setPositiveNumber(_elem: Element, value: string, subtract?: number) {

  // Any relative (+/-) values have already been
  // normalized at this point
  var matches = rcssNum.exec(value);
  return matches ?

    // Guard against undefined "subtract", e.g., when used as in cssHooks
    Math.max(0, +matches[2] - (subtract || 0)) + (matches[3] || "px") :
    value;
}
