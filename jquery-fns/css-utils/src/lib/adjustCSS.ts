import { rcssNum } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/var';

import proto from './proto';
import { isAutoPx } from './shared';

proto.adjustCSS = (doc, elem, prop, valueParts) => {
  var adjusted, scale,
    maxIterations = 20,
    currentValue =
      function () {
        return proto.css(doc, elem, prop, "");
      },
    initial = currentValue(),
    unit = valueParts && valueParts[3] || (isAutoPx(prop) ? "px" : "");

  // Starting value computation is required for potential unit mismatches
  let initialInUnit: any = elem.nodeType &&
    (!isAutoPx(prop) || unit !== "px" && +initial) &&
    rcssNum.exec(proto.css(doc, elem, prop));

  if (initialInUnit && initialInUnit[3] !== unit) {

    // Support: Firefox <=54 - 66+
    // Halve the iteration target value to prevent interference from CSS upper bounds (gh-2144)
    initial = initial / 2;

    // Trust units reported by jQuery.css
    unit = unit || initialInUnit[3];

    // Iteratively approximate from a nonzero starting point
    initialInUnit = +initial || 1;

    while (maxIterations--) {

      // Evaluate and update our best guess (doubling guesses that zero out).
      // Finish if the scale equals or crosses 1 (making the old*new product non-positive).
      proto.style(doc, elem, prop, initialInUnit + unit);
      if ((1 - scale) * (1 - (scale = currentValue() / initial || 0.5)) <= 0) {
        maxIterations = 0;
      }
      initialInUnit = initialInUnit / scale;

    }

    initialInUnit = initialInUnit * 2;
    proto.style(doc, elem, prop, initialInUnit + unit);

    // Make sure we update the tween properties later on
    valueParts = valueParts || [];
  }

  if (valueParts) {
    initialInUnit = +initialInUnit || +initial || 0;

    // Apply relative offset (+=/-=) if specified
    adjusted = valueParts[1] ?
      initialInUnit + (valueParts[1] + 1) * valueParts[2] :
      +valueParts[2];
  }
  return adjusted;
}
