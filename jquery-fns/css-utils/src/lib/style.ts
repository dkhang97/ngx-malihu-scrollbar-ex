import { rcssNum } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/var';

import proto from './proto';
import { cssCamelCase, cssHooks, isAutoPx, isIECheck } from './shared';

proto.style = (doc, elem, name, value, extra) => {

  // Don't set styles on text and comment nodes
  if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem['style']) {
    return;
  }

  // Make sure that we're working with the right name
  var ret, origName = cssCamelCase(name),
    style: CSSStyleDeclaration = elem['style'];


  // Gets hook for the prefixed version, then unprefixed version
  const hooks: (typeof cssHooks)[''] = cssHooks[name] || cssHooks[origName];

  // Check if we're setting a value
  if (value !== undefined) {
    let type = typeof value;

    // Convert "+=" or "-=" to relative numbers (#7345)
    if (type === "string" && (ret = rcssNum.exec(value)) && ret[1]) {
      value = proto.adjustCSS(doc, elem, name, ret);

      // Fixes bug #9237
      type = "number";
    }

    // Make sure that null and NaN values aren't set (#7116)
    if (value == null || value !== value) {
      return;
    }

    // If the value is a number, add `px` for certain CSS properties
    if (type === "number") {
      value += ret && ret[3] || (isAutoPx(origName) ? "px" : "");
    }

    // Support: IE <=9 - 11+
    // background-* props of a cloned element affect the source element (#8908)
    if (isIECheck(doc) && value === "" && name.indexOf("background") === 0) {
      style[name] = "inherit";
    }

    // If a hook was provided, use that value, otherwise just set the specified value
    if (!hooks || !("set" in hooks) ||
      (value = hooks.set(doc, elem, value, extra)) !== undefined) {

      style[name] = value;
    }

  } else {
    // If a hook was provided get the non-computed value from there
    if (hooks && "get" in hooks &&
      (ret = hooks.get(doc, elem, false, extra)) !== undefined) {

      return ret;
    }

    // Otherwise just get the value from the style object
    return style[name];
  }

}


