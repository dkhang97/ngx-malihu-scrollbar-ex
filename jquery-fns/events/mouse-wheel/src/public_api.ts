import { $create } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';
import { createEventRegistry, WrapperEvent } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/event';

let nullLowestDeltaTimeout: number, lowestDelta: number;

const DATA_LINE_HEIGHT = 'mousewheel-line-height';
const DATA_PAGE_HEIGHT = 'mousewheel-page-height';

const special = {
  settings: {
    adjustOldDeltas: true, // see shouldAdjustOldDeltas() below
    normalizeOffset: true  // calls getBoundingClientRect for each event
  }
}

const sourceEvents = (doc: Document) => ('onwheel' in doc || doc['documentMode'] >= 9) ?
  ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll']

export const $event = createEventRegistry(({ document }) => sourceEvents(document).map(type => ({ type, passive: true })), 'mousewheel', {
  converter(sourceEvent: Event): [Event, number, number, number] {
    const orgEvent = sourceEvent || <any>window.event;

    let delta = 0, deltaX = 0, deltaY = 0,
      absDelta = 0,
      offsetX = 0,
      offsetY = 0;

    const event = WrapperEvent.create(sourceEvent, 'mousewheel');

    // event = $.event['fix'](orgEvent);
    // event.type = 'mousewheel';

    // Old school scrollwheel delta
    if ('detail' in orgEvent) { deltaY = orgEvent.detail * -1; }
    if ('wheelDelta' in orgEvent) { deltaY = orgEvent.wheelDelta; }
    if ('wheelDeltaY' in orgEvent) { deltaY = orgEvent.wheelDeltaY; }
    if ('wheelDeltaX' in orgEvent) { deltaX = orgEvent.wheelDeltaX * -1; }

    // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
    if ('axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS) {
      deltaX = deltaY * -1;
      deltaY = 0;
    }

    // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
    delta = deltaY === 0 ? deltaX : deltaY;

    // New school wheel delta (wheel event)
    if ('deltaY' in orgEvent) {
      deltaY = orgEvent.deltaY * -1;
      delta = deltaY;
    }
    if ('deltaX' in orgEvent) {
      deltaX = orgEvent.deltaX;
      if (deltaY === 0) { delta = deltaX * -1; }
    }

    // No change actually happened, no reason to go any further
    if (deltaY === 0 && deltaX === 0) { return; }

    // Need to convert lines and pages to pixels if we aren't already in pixels
    // There are three delta modes:
    //   * deltaMode 0 is by pixels, nothing to do
    //   * deltaMode 1 is by lines
    //   * deltaMode 2 is by pages
    if (orgEvent.deltaMode === 1) {
      const lineHeight = +this.extendUtils($data).data(DATA_LINE_HEIGHT);
      delta *= lineHeight;
      deltaY *= lineHeight;
      deltaX *= lineHeight;
    } else if (orgEvent.deltaMode === 2) {
      var pageHeight = +this.extendUtils($data).data(DATA_PAGE_HEIGHT);
      delta *= pageHeight;
      deltaY *= pageHeight;
      deltaX *= pageHeight;
    }

    // Store lowest absolute delta to normalize the delta values
    absDelta = Math.max(Math.abs(deltaY), Math.abs(deltaX));

    if (!lowestDelta || absDelta < lowestDelta) {
      lowestDelta = absDelta;

      // Adjust older deltas if necessary
      if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
        lowestDelta /= 40;
      }
    }

    // Adjust older deltas if necessary
    if (shouldAdjustOldDeltas(orgEvent, absDelta)) {
      // Divide all the things by 40!
      delta /= 40;
      deltaX /= 40;
      deltaY /= 40;
    }

    // Get a whole, normalized value for the deltas
    delta = Math[delta >= 1 ? 'floor' : 'ceil'](delta / lowestDelta);
    deltaX = Math[deltaX >= 1 ? 'floor' : 'ceil'](deltaX / lowestDelta);
    deltaY = Math[deltaY >= 1 ? 'floor' : 'ceil'](deltaY / lowestDelta);

    // Normalise offsetX and offsetY properties
    if (special.settings.normalizeOffset && this[0]?.getBoundingClientRect) {
      var boundingRect = this[0].getBoundingClientRect();
      offsetX = event.clientX - boundingRect.left;
      offsetY = event.clientY - boundingRect.top;
    }

    // Add information to the event object
    event.deltaX = deltaX;
    event.deltaY = deltaY;
    event.deltaFactor = lowestDelta;
    event.offsetX = offsetX;
    event.offsetY = offsetY;
    // Go ahead and set deltaMode to 0 since we converted to pixels
    // Although this is a little odd since we overwrite the deltaX/Y
    // properties with normalized deltas.
    event.deltaMode = 0;

    // Clearout lowestDelta after sometime to better
    // handle multiple device types that give different
    // a different lowestDelta
    // Ex: trackpad = 3 and mouse wheel = 120
    if (nullLowestDeltaTimeout) { clearTimeout(nullLowestDeltaTimeout); }
    nullLowestDeltaTimeout = <any>setTimeout(nullLowestDelta, 200);

    return [event as any, delta, deltaX, deltaY];
  },
  setup(wrapper) {
    const $w = wrapper.extendUtils($data, $css);

    let $parent = wrapper.offsetParent().extendUtils($css);
    if ($parent.elements.length) {
      $parent = $create(wrapper.document, $css)('body');
    }
    const lineHeight = parseInt($parent.css('fontSize'), 10) || parseInt($w.css('fontSize'), 10) || 16;


    // Store the line height and page height for this particular element
    $w.data(DATA_LINE_HEIGHT, lineHeight);
    $w.data(DATA_PAGE_HEIGHT, wrapper.extendUtils($dimensions).height());
  },
});


function nullLowestDelta() {
  lowestDelta = null;
}

function shouldAdjustOldDeltas(orgEvent, absDelta: number) {
  // If this is an older event and the delta is divisable by 120,
  // then we are assuming that the browser is treating this as an
  // older mouse wheel event and that we should divide the deltas
  // by 40 to try and get a more usable deltaFactor.
  // Side note, this actually impacts the reported scroll distance
  // in older browsers and can cause scrolling to be slower than native.
  // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
  return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
}
