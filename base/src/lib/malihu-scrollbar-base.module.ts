import { ModuleWithProviders, NgModule } from '@angular/core';
import { MALIHU_CONFIG_TOKEN, MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';

import { MalihuScrollbarDirective } from './malihu-scrollbar.directive';

@NgModule({
  exports: [MalihuScrollbarDirective],
  declarations: [MalihuScrollbarDirective],
})
export class MalihuScrollbarBaseModule {
  static forRoot(options?: MalihuScrollbarOptions): ModuleWithProviders<MalihuScrollbarBaseModule> {
    return {
      ngModule: MalihuScrollbarBaseModule,
      providers: [{ provide: MALIHU_CONFIG_TOKEN, useValue: options }]
    };
  }
}
