import { AfterViewInit, Directive, EventEmitter, Injector, OnDestroy, Output } from '@angular/core';
import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { Subscription } from 'rxjs';

import { MalihuScrollbarBase } from './malihu-scrollbar-base';

@Directive({
  selector: '[malihu-scrollbar]',
  exportAs: 'malihuScrollbar',
  inputs: ['options: malihu-scrollbar']
})
export class MalihuScrollbarDirective extends MalihuScrollbarBase
  implements AfterViewInit, OnDestroy {
  private _subscriptions: Subscription[] = [];

  @Output()
  readonly malihuInitialized = new EventEmitter<JQueryFnsWrapper>();

  constructor(injector: Injector) { super(injector); }

  ngOnDestroy() {
    this._subscriptions.forEach(item => item.unsubscribe());
    this.destroy();
  }
  ngAfterViewInit() {
    this._subscriptions.push(this.onScrollInit($el => this.malihuInitialized.emit($el)));
    if (this.$el.is(':visible')) {
      this.initScrollbar();
    }
  }
}
