import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { ElementRef, Injector, NgZone, PLATFORM_ID } from '@angular/core';
import {
  MALIHU_CONFIG_TOKEN,
  MalihuScrollbarOptions,
  ScrollToParameter,
  ScrollToParameterOptions,
} from 'ngx-malihu-scrollbar-ex/declarations';
import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $pseudo } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';
import { $_visible } from 'ngx-malihu-scrollbar-ex/jquery-fns/pseudos';
import * as malihu from 'ngx-malihu-scrollbar-ex/jquery-malihu';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter, take } from 'rxjs/operators';

import type { CreateWrapperQueryReturn } from 'ngx-malihu-scrollbar-ex/jquery-fns/core';


export class MalihuScrollbarBase {
  readonly elementRef: ElementRef;
  readonly defaultOptions: MalihuScrollbarOptions
    = this.injector.get(MALIHU_CONFIG_TOKEN);
  protected readonly zone = this.injector.get(NgZone, null);
  protected readonly doc = this.injector.get(DOCUMENT, null);
  readonly platformId = this.injector?.get(PLATFORM_ID, null);

  readonly $ = $create(this.doc, $data, $pseudo);
  readonly $el: ReturnType<MalihuScrollbarBase['$']>;
  readonly #scrollInitSbj = new BehaviorSubject<JQueryFnsWrapper>(null);

  options: MalihuScrollbarOptions;

  get calculatedOptions() {
    return Object.assign({}, this.defaultOptions, this.options)
  }

  constructor(
    protected readonly injector: Injector,
    elementRef?: ElementRef,
    options?: MalihuScrollbarOptions,
  ) {
    this.elementRef = elementRef || this.injector.get(ElementRef);
    const platformId = this.platformId;
    this.$el = (platformId === null || isPlatformBrowser(platformId))
      && this.$(this.elementRef.nativeElement);
    this.options = options;
    $_visible.register();
  }

  onScrollInit(cb: ($el: JQueryFnsWrapper) => void): Subscription {
    return this.#scrollInitSbj.pipe(filter(sn => !!sn), take(1)).subscribe($el => cb($el));
  }

  initScrollbar() {
    const $el = this.$el;

    if ($el) {
      this.runOutsideAngular(() => {
        malihu.init($el, this.calculatedOptions);
        this.#scrollInitSbj.next($el);
      });

      if ($el.length > 0 && $el[0]['tagName'] === 'BODY') {
        $el.extendUtils($css).css({
          position: 'absolute',
          overflow: 'auto',
          width: '100vw',
          height: '100vh'
        });
      }
    }

    return this;
  }

  scrollTo(parameter: ScrollToParameter | (() => ScrollToParameter), options?: ScrollToParameterOptions) {
    malihu.scrollTo(this.$el, parameter, options)
  }

  update() {
    malihu.update(this.$el);
  }

  stop() {
    malihu.stop(this.$el);
  }

  disable(reset?: boolean) {
    malihu.disable(this.$el, !!reset);
  }

  destroy() {
    malihu.destroy(this.$el);
  }

  private runOutsideAngular(task: () => void) {
    const zn = this.zone;
    zn ? zn.runOutsideAngular(() => task()) : setTimeout(() => task(), 0);
  }
}

