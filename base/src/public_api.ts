export { MalihuScrollbarBase } from './lib/malihu-scrollbar-base';
export { MalihuScrollbarDirective } from './lib/malihu-scrollbar.directive';
export { MalihuScrollbarBaseModule } from './lib/malihu-scrollbar-base.module';
