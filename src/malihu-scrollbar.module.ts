import { ModuleWithProviders, NgModule } from '@angular/core';
import { MALIHU_CONFIG_TOKEN, MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';

import { MalihuScrollbarComponent } from './malihu-scrollbar.component';
import { MalihuScrollbarBaseModule } from 'ngx-malihu-scrollbar-ex/base';

@NgModule({
  exports: [MalihuScrollbarComponent, MalihuScrollbarBaseModule],
  declarations: [MalihuScrollbarComponent],
})
export class MalihuScrollbarModule {
  static forRoot(options?: MalihuScrollbarOptions): ModuleWithProviders<MalihuScrollbarModule> {
    return {
      ngModule: MalihuScrollbarModule,
      providers: [{ provide: MALIHU_CONFIG_TOKEN, useValue: options }]
    };
  }
}
