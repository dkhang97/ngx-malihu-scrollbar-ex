import { AfterViewInit, Component, EventEmitter, Injector, OnDestroy, Output, ViewEncapsulation } from '@angular/core';
import { MalihuScrollbarBase } from 'ngx-malihu-scrollbar-ex/base';
import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { Subscription } from 'rxjs';


@Component({
  selector: '[malihu-component]',
  template: '<ng-content></ng-content>',
  styleUrls: ['../styles/malihu-scrollbar.scss'],
  encapsulation: ViewEncapsulation.None,
  inputs: ['options: malihu-component'],
  exportAs: 'malihuComponent',
})
export class MalihuScrollbarComponent extends MalihuScrollbarBase
  implements AfterViewInit, OnDestroy {
  private _subscriptions: Subscription[] = [];

  @Output()
  readonly malihuInitialized = new EventEmitter<JQueryFnsWrapper>();

  constructor(injector: Injector) { super(injector); }

  ngOnDestroy() {
    this._subscriptions.forEach(item => item.unsubscribe());
    this.destroy();
  }
  ngAfterViewInit() {
    this._subscriptions.push(this.onScrollInit($el => this.malihuInitialized.emit($el)));
    if (this.$el.is(':visible')) {
      this.initScrollbar();
    }
  }
}
