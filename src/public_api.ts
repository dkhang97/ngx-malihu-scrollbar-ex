export {
  ScrollToParameterOptions as ScrollToOptions,
  MalihuScrollbarOptions, ScrollToParameter, MALIHU_CONFIG_TOKEN
} from 'ngx-malihu-scrollbar-ex/declarations';
export { MalihuScrollbarBase, MalihuScrollbarDirective } from 'ngx-malihu-scrollbar-ex/base';

export { MalihuScrollbarComponent } from './malihu-scrollbar.component';
export { MalihuScrollbarModule } from './malihu-scrollbar.module';
