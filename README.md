# ngx-malihu-scrollbar-ex
[![version](https://img.shields.io/npm/v/ngx-malihu-scrollbar-ex.svg?style=flat)](https://www.npmjs.com/package/ngx-malihu-scrollbar-ex) [![npm](https://img.shields.io/npm/l/ngx-malihu-scrollbar-ex.svg)](https://opensource.org/licenses/MIT)

### Table of contents

- [Installation](#installation)
- [Usage](#usage)

## Installation

Use the following command to add ngx-malihu-scrollbar-ex library to your `package.json` file.

```bash
npm i ngx-malihu-scrollbar-ex --save
```

## Usage

You must import `MalihuScrollbarModule` inside your module to be able to use `malihu-scrollbar` directive

```diff
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
+ import { MalihuScrollbarModule } from 'ngx-malihu-scrollbar-ex';


@NgModule({
  imports: [
    CommonModule,
+   MalihuScrollbarModule.forRoot(),
  ],
})
```

You can also pass the default options to `MalihuScrollbarModule`

```typescript
MalihuScrollbarModule.forRoot({
    axis: 'y',
    theme: 'minimal-dark',
    scrollInertia: 50,
    mouseWheel: { preventDefault: true }
}),
```

ngx-malihu-scrollbar-ex provides a directive to apply the custom scrollbar on your HTML element.

> For a complete list of available customization options please refer to the original [Malihu Custom Scrollbar documentation](http://manos.malihu.gr/jquery-custom-content-scroller/).

---

### Component

You can use `malihu-component` component directly on an HTML element, ~~CSS file in **angular.json** is required here~~ that's all!

#### home.component.html

```html
<div malihu-component #mlh="malihuComponent" (click)="mlh.scrollTo('bottom')">
   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
</div>
```

---

### Directive

You can use `malihu-scrollbar` directive directly on an HTML element

You will need to add Malihu Custom Scrollbar CSS files to your application.

If you are using [Angular CLI](https://cli.angular.io/) you can follow the example below...

#### .angular-cli.json

```diff
"styles": [
+ "node_modules/ngx-malihu-scrollbar-ex/malihu-scrollbar.css",
  "styles.scss",
],
```

#### home.component.html
```html
<div malihu-scrollbar>
   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
</div>
```

You can also provide plugin options by passing that options to its property.

#### home.component.ts
```typescript
public scrollbarOptions = { axis: 'y', theme: 'minimal' };
```

#### home.component.html
```html
<div [malihu-scrollbar]="scrollbarOptions">
   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
</div>
```

To use the `malihu-scrollbar` directive in angular template html

```html
<div malihu-scrollbar #mlh="malihuScrollbar" (click)="mlh.disable()">
   Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua...
</div>
```
