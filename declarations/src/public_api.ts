import { InjectionToken } from '@angular/core';

import { AutoExpandHorizontalScroll, MalihuScrollbarOptions } from './malihu-scrollbar-options';

export {
  AutoExpandHorizontalScroll,
  MalihuScrollbarOptions,
}

export const MALIHU_CONFIG_TOKEN = new InjectionToken<MalihuScrollbarOptions>('ngx-malihu-scrollbar-ex-config');

export type ScrollToParameter = string | number | [number, number] | HTMLElement | { x?: number, y?: number };



export interface ScrollToParameterOptions {
  /**
  * Scroll-to animation speed, value in milliseconds
  */
  scrollInertia?: number;
  /**
  * Scroll-to animation easing, values: "linear", "easeOut", "easeInOut".
  */
  scrollEasing?: string;
  /**
  * Scroll scrollbar dragger (instead of content) to a number of pixels, values: true, false
  */
  moveDragger?: boolean;
  /**
  * Set a timeout for the method (the default timeout is 60 ms in order to work with automatic scrollbar update), value in milliseconds.
  */
  timeout?: number;
  /**
  * Trigger user defined callback after scroll-to completes, value: true, false
  */
  callbacks?: boolean;
}
