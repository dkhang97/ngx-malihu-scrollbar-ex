/*jshint esversion: 9 */

const fs = require('fs');
const fsPath = require('path');

function resolveDist(path) {
  return fsPath.join('./dist', path);
}

async function writeFile(path, content) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, content, err => {
      if (err) {
        reject(err);
        return;

      }
      console.log('Wrote ' + (fsPath.isAbsolute(path) ? path : fsPath.resolve(path)));
      resolve();
    });
  });
}

(async () => {

  let jsContent = fs.readFileSync(resolveDist('esm2015/malihu-scrollbar.component.js')).toString();

  const decorMatch = /MalihuScrollbarComponent\.decorators\s*=\s*/.exec(jsContent);

  jsContent = jsContent.substring(decorMatch.index + decorMatch[0].length);

  const endBracketMatch = /\];/.exec(jsContent);

  jsContent = jsContent.substring(0, endBracketMatch.index + endBracketMatch[0].length);

  const [{ args: [{ styles }] }] = eval(`var Component=null;var ViewEncapsulation={};${jsContent}`);

  const outputFile = resolveDist('malihu-scrollbar.css');
  await writeFile(outputFile, styles.join('\n'));

  const sass = require('node-sass');

  function resolveStylesDir(path) {
    return resolveDist(fsPath.join('styles', path));
  }

  async function compileScss(path) {
    return new Promise((resolve, reject) => {
      sass.render({ file: resolveStylesDir(path), outputStyle: 'compressed' }, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result.css.toString());
        }
      });
    });
  }

  const baseStyle = await compileScss('malihu-scrollbar.base.scss');
  await writeFile(resolveStylesDir('malihu-scrollbar.base.css'), baseStyle);

  const themes = fs.readdirSync(resolveStylesDir('themes'))
    .map(file => {
      const lw = file.toLowerCase();
      if (lw.startsWith('_') && lw.endsWith('.scss')) {
        return lw.substr(1, lw.length - 6);
      }
    }).filter(f => !!f && !f.endsWith('.base'));

  await Promise.all(themes.map(theme => new Promise(resolve => {
    const stylePath = resolveStylesDir('themes/' + theme + '.css');

    fs.access(stylePath, err => {
      if (err) {
        resolve();
        return;
      }

      fs.unlink(stylePath, () => resolve());
      // stat.
    });
  })));

  const themeOutputs = await Promise.all(themes.map(async theme => {
    const themeCss = await compileScss('themes/_' + theme + '.scss');
    return [
      { file: resolveStylesDir('themes/' + theme + '.css'), content: themeCss },
      { file: resolveStylesDir('malihu-scrollbar.' + theme + '.css'), content: baseStyle + themeCss },
    ];
  }));

  const outputTasks = themeOutputs.map(outputs =>
    Promise.all(outputs.map(output => writeFile(output.file, output.content))));
  await Promise.all(outputTasks);
})();
