const stateMap: Record<string, WeakMap<any, any>> = {};

export function retrieveState<T>(stateName: string, key: any, creator: () => T) {
  const state = stateMap[stateName] ?? (stateMap[stateName] = new WeakMap());

  if (state.has(key)) {
    return state.get(key);
  }

  const val = creator();
  state.set(key, val);
  return val;
}
