import { AutoExpandHorizontalScroll, MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions, $event, $offset, $pseudo } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';
import { WrappedEvent } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders/event';

import { retrieveState } from '../state';
import { classMap, pluginNS, pluginPfx } from '../variables';
import { _canAccessIFrame } from './iframe';

/* -------------------- */


/* changes options according to theme */
export function _theme(obj: any) {
  obj.autoDraggerLength = ~["rounded", "rounded-dark", "rounded-dots", "rounded-dots-dark"].indexOf(obj.theme)
    ? false : obj.autoDraggerLength;

  obj.autoExpandScrollbar = ~["rounded-dots", "rounded-dots-dark", "3d", "3d-dark",
    "3d-thick", "3d-thick-dark", "inset", "inset-dark", "inset-2",
    "inset-2-dark", "inset-3", "inset-3-dark"].indexOf(obj.theme) ? false : obj.autoExpandScrollbar;

  obj.scrollButtons.enable = ~["minimal", "minimal-dark"].indexOf(obj.theme)
    ? false : obj.scrollButtons.enable;

  obj.autoHideScrollbar = ~["minimal", "minimal-dark"].indexOf(obj.theme)
    ? true : obj.autoHideScrollbar;

  obj.scrollbarPosition = ~["minimal", "minimal-dark"].indexOf(obj.theme)
    ? "outside" : obj.scrollbarPosition;
};

/* normalizes axis option to valid values: "y", "x", "yx" */
export function _findAxis(val: string) {
  return ["yx", "xy", "auto"].includes(val) ? "yx" : ["x", "horizontal"].includes(val) ? "x" : "y";
}
/* -------------------- */


/* normalizes scrollButtons.scrollType option to valid values: "stepless", "stepped" */
export function _findScrollButtonsType(val: string) {
  return (val === "stepped" || val === "pixels" || val === "step" || val === "click") ? "stepped" : "stepless";
}
/* -------------------- */


/* generates plugin markup */
export function _pluginMarkup($this: JQueryFnsWrapper) {
  const $thisExtra = $this.extendUtils($css, $dimensions);

  var d = $this.extendUtils($data).data(pluginPfx), o: MalihuScrollbarOptions = d.opt,
    expandClass = o.autoExpandScrollbar ? ` ${classMap.scrollTools.onDrag}_expand` : "",
    scrollbar = [
      `<div id='mCSB_${d.idx}_scrollbar_vertical' class='mCSB_scrollTools mCSB_${d.idx}_scrollbar mCS-${o.theme} mCSB_scrollTools_vertical${expandClass}'>`
      + `<div class='${classMap.dragger.container}'><div id='mCSB_${d.idx}_dragger_vertical' class='mCSB_dragger' style='position:absolute;'>`
      + `<div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>`,
      `<div id='mCSB_${d.idx}_scrollbar_horizontal' class='mCSB_scrollTools mCSB_${d.idx}_scrollbar mCS-${o.theme} mCSB_scrollTools_horizontal${expandClass}'>`
      + `<div class='${classMap.dragger.container}'><div id='mCSB_${d.idx}_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'>`
      + `<div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>`
    ],
    wrapperClass = o.axis === "yx" ? "mCSB_vertical_horizontal" : o.axis === "x" ? "mCSB_horizontal" : "mCSB_vertical",
    scrollbars = o.axis === "yx" ? scrollbar[0] + scrollbar[1] : o.axis === "x" ? scrollbar[1] : scrollbar[0],
    contentWrapper = o.axis === "yx" ? `<div id='mCSB_${d.idx}_container_wrapper' class='mCSB_container_wrapper' />` : "",
    autoHideClass = o.autoHideScrollbar ? ` ${classMap.autoHide}` : "",
    scrollbarDirClass = (o.axis !== "x" && d.langDir === "rtl") ? ` ${classMap.rtl}` : "";
  if (o.setWidth) { $thisExtra.css("width", o.setWidth); } /* set element width */
  if (o.setHeight) { $thisExtra.css("height", o.setHeight); } /* set element height */
  o.setLeft = (o.axis !== "y" && d.langDir === "rtl") ? "989999px" : o.setLeft; /* adjust left position for rtl direction */
  $thisExtra.addClass(`${pluginNS} _${pluginPfx}_${d.idx}${autoHideClass}${scrollbarDirClass}`);

  const wrapInnerHtml = `<div id='mCSB_${d.idx}' class='mCustomScrollBox mCS-${o.theme} ${wrapperClass}'>`
    + `<div id='mCSB_${d.idx}_container' class='mCSB_container' style='position:relative; top:${o.setTop}; left:${o.setLeft};' dir='${d.langDir}' /></div>`;

  $this.children().wrapAll(wrapInnerHtml);


  var mCustomScrollBox = $thisExtra.selectNew(`#mCSB_${d.idx}`),
    mCSB_container = $thisExtra.selectNew(`#mCSB_${d.idx}_container`);
  if (o.axis !== "y" && !o.advanced.autoExpandHorizontalScroll) {
    mCSB_container.css("width", _contentWidth($thisExtra.selectNew(mCSB_container[0])));
  }
  if (o.scrollbarPosition === "outside") {
    if ($thisExtra.css("position") === "static") { /* requires elements with non-static position */
      $thisExtra.css("position", "relative");
    }
    $thisExtra.css("overflow", "visible");
    mCustomScrollBox.addClass("mCSB_outside");
    mCustomScrollBox.after(scrollbars);
  } else {
    mCustomScrollBox.addClass("mCSB_inside");
    mCustomScrollBox.append(scrollbars);
    mCSB_container.wrap(contentWrapper);
  }
  _scrollButtons($this); /* add scrollbar buttons */
  /* minimum dragger length */
  var mCSB_dragger = [$thisExtra.selectNew(`#mCSB_${d.idx}_dragger_vertical`), $thisExtra.selectNew(`#mCSB_${d.idx}_dragger_horizontal`)];
  mCSB_dragger[0].css("min-height", mCSB_dragger[0].height());
  mCSB_dragger[1].css("min-width", mCSB_dragger[1].width());
}
/* -------------------- */


/* calculates content width */
export function _contentWidth($this: JQueryFnsWrapper) {
  const $thisExtra = $this.extendUtils($dimensions);
  var val = [$this[0].scrollWidth, Math.max(...$this.children().map(function () { return $thisExtra.selectNew(this).outerWidth(true); }))], w = $thisExtra.parent().width();
  return val[0] > w ? val[0] : val[1] > w ? val[1] : "100%";
}
/* -------------------- */


/* expands content horizontally */
export function _expandContentHorizontally($this: JQueryFnsWrapper) {
  var d = $this.extendUtils($data).data(pluginPfx), o: MalihuScrollbarOptions = d.opt,
    mCSB_container = $this.selectNew(`#mCSB_${d.idx}_container`).extendUtils($css, $dimensions);
  if (o.advanced.autoExpandHorizontalScroll && o.axis !== "y") {
    /* calculate scrollWidth */
    mCSB_container.css({ "width": "auto", "min-width": 0, "overflow-x": "scroll" });
    var w = Math.ceil(mCSB_container[0].scrollWidth);
    if (o.advanced.autoExpandHorizontalScroll === AutoExpandHorizontalScroll.FORCE_SCROLL ||
      (o.advanced.autoExpandHorizontalScroll !== AutoExpandHorizontalScroll.NON_SCROLL && w > mCSB_container.parent().width())) {
      mCSB_container.css({ "width": w, "min-width": "100%", "overflow-x": "inherit" });
    } else {
      /*
      wrap content with an infinite width div and set its position to absolute and width to auto.
      Setting width to auto before calculating the actual width is important!
      We must let the browser set the width as browser zoom values are impossible to calculate.
      */
      mCSB_container.css({ "overflow-x": "inherit", "position": "absolute" });
      mCSB_container.wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />");
      mCSB_container.css({ /* set actual width, original position and un-wrap */
        /*
        get the exact width (with decimals) and then round-up.
        Using jquery outerWidth() will round the width value which will mess up with inner elements that have non-integer width
        */
        "width": (Math.ceil(mCSB_container[0].getBoundingClientRect().right + 0.4) - Math.floor(mCSB_container[0].getBoundingClientRect().left)),
        "min-width": "100%",
        "position": "relative"
      })
      mCSB_container.unwrap();
    }
  }
}
/* -------------------- */


/* adds scrollbar buttons */
export function _scrollButtons($this: JQueryFnsWrapper) {
  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt,
    mCSB_scrollTools = $this.selectNew(`.mCSB_${d.idx}_scrollbar:first`),
    tabindex = !_isNumeric(o.scrollButtons.tabindex) ? "" : "tabindex='" + o.scrollButtons.tabindex + "'",
    btnHTML = [
      `<a href='#' class='${classMap.button.up}' ${tabindex} />`,
      `<a href='#' class='${classMap.button.down}' ${tabindex} />`,
      `<a href='#' class='${classMap.button.left}' ${tabindex} />`,
      `<a href='#' class='${classMap.button.right}' ${tabindex} />`
    ],
    btn = [(o.axis === "x" ? btnHTML[2] : btnHTML[0]), (o.axis === "x" ? btnHTML[3] : btnHTML[1]), btnHTML[2], btnHTML[3]];
  if (o.scrollButtons.enable) {
    mCSB_scrollTools.prepend(btn[0]).append(btn[1]).next(".mCSB_scrollTools").prepend(btn[2]).append(btn[3]);
  }
}
/* -------------------- */


/* checks if content overflows its container to determine if scrolling is required */
export function _overflowed($this: JQueryFnsWrapper) {
  const $thisExtra = $this.extendUtils($data, $dimensions);

  const d = $thisExtra.data(pluginPfx),
    mCustomScrollBox = $thisExtra.selectNew(`#mCSB_${d.idx}`),
    mCSB_container = $thisExtra.selectNew(`#mCSB_${d.idx}_container`);

  let contentHeight = d.overflowed == null ? mCSB_container.height() : mCSB_container.outerHeight(false),
    contentWidth = d.overflowed == null ? mCSB_container.width() : mCSB_container.outerWidth(false),
    h = mCSB_container[0].scrollHeight, w = mCSB_container[0].scrollWidth;

  if (h > contentHeight) { contentHeight = h; }
  if (w > contentWidth) { contentWidth = w; }
  contentHeight += Math.abs(mCSB_container[0].offsetTop);
  contentWidth += Math.abs(mCSB_container[0].offsetLeft);
  return [contentHeight > Math.ceil(mCustomScrollBox.height()), contentWidth > Math.ceil(mCustomScrollBox.width())];
}
/* -------------------- */



/* toggles scrollbar visibility */
export function _scrollbarVisibility($this: JQueryFnsWrapper, disabled?: boolean) {
  const $thisExtra = $this.extendUtils($data, $css);

  var d = $thisExtra.data(pluginPfx), o = d.opt,
    contentWrapper = $thisExtra.selectNew(`#mCSB_${d.idx}_container_wrapper`),
    content = contentWrapper.length ? contentWrapper : $thisExtra.selectNew(`#mCSB_${d.idx}_container`),
    scrollbar = [$thisExtra.selectNew(`#mCSB_${d.idx}_scrollbar_vertical`), $thisExtra.selectNew(`#mCSB_${d.idx}_scrollbar_horizontal`)],
    mCSB_dragger = [scrollbar[0].find(".mCSB_dragger"), scrollbar[1].find(".mCSB_dragger")];
  if (o.axis !== "x") {
    if (d.overflowed[0] && !disabled) {
      scrollbar[0].add(mCSB_dragger[0]).add(scrollbar[0].children("a")).css("display", "block");
      content.removeClass(classMap.noScrollBar.y + " " + classMap.hidden.y);
    } else {
      if (o.alwaysShowScrollbar) {
        if (o.alwaysShowScrollbar !== 2) { mCSB_dragger[0].css("display", "none"); }
        content.removeClass(classMap.hidden.y);
      } else {
        scrollbar[0].css("display", "none");
        content.addClass(classMap.hidden.y);
      }
      content.addClass(classMap.noScrollBar.y);
    }
  }
  if (o.axis !== "y") {
    if (d.overflowed[1] && !disabled) {
      scrollbar[1].add(mCSB_dragger[1]).add(scrollbar[1].children("a")).css("display", "block");
      content.removeClass(`${classMap.noScrollBar.x} ${classMap.hidden.x}`);
    } else {
      if (o.alwaysShowScrollbar) {
        if (o.alwaysShowScrollbar !== 2) { mCSB_dragger[1].css("display", "none"); }
        content.removeClass(classMap.hidden.x);
      } else {
        scrollbar[1].css("display", "none");
        content.addClass(classMap.hidden.x);
      }
      content.addClass(classMap.noScrollBar.x);
    }
  }
  if (!d.overflowed[0] && !d.overflowed[1]) {
    $thisExtra.addClass(classMap.noScrollBar.all);
  } else {
    $thisExtra.removeClass(classMap.noScrollBar.all);
  }
}
/* -------------------- */


/* returns input coordinates of pointer, touch and mouse events (relative to document) */
export function _coordinates(doc: Document, e: WrappedEvent<MouseEvent | TouchEvent>): [number, number, boolean] {
  const $w = $create(doc, $offset);

  var t = e.type, o = e.target['ownerDocument'] !== doc && frameElement !== null ? [$w(frameElement).offset().top, $w(frameElement).offset().left] : null,
    io = _canAccessIFrame() && e.target['ownerDocument'] !== top.document && frameElement !== null ? [$w(e.view.frameElement).offset().top, $w(e.view.frameElement).offset().left] : [0, 0];
  switch (t) {
    case "pointerdown": case "MSPointerDown": case "pointermove": case "MSPointerMove": case "pointerup": case "MSPointerUp":
      const orgMouseEvent: MouseEvent = e.originalEvent as any;
      return o ? [orgMouseEvent.pageY - o[0] + io[0], orgMouseEvent.pageX - o[1] + io[1], false] : [orgMouseEvent.pageY, orgMouseEvent.pageX, false];
    case "touchstart": case "touchmove": case "touchend":
      const orgTouchEvent: TouchEvent = e.originalEvent as any;
      var touch = orgTouchEvent.touches[0] || orgTouchEvent.changedTouches[0],
        touches = orgTouchEvent.touches.length || orgTouchEvent.changedTouches.length;
      return e.target['ownerDocument'] !== doc ? [touch.screenY, touch.screenX, touches > 1] : [touch.pageY, touch.pageX, touches > 1];
    default:
      const mouseEv: WrappedEvent<MouseEvent> = e as any;
      return o ? [mouseEv.pageY - o[0] + io[0], mouseEv.pageX - o[1] + io[1], false] : [mouseEv.pageY, mouseEv.pageX, false];
  }
}
/* -------------------- */


/* disables mouse-wheel when hovering specific elements like select, datalist etc. */
export function _disableMousewheel(el: JQueryFnsWrapper, target: HTMLElement) {
  const tag = target.nodeName.toLowerCase(),
    tags: string[] = el.extendUtils($data).data(pluginPfx).opt.mouseWheel.disableOver,
    /* elements that require focus */
    focusTags = ["select", "textarea"];
  return (~(tags?.indexOf(tag) ?? -1) || ~(tags || []).findIndex(t => el.selectNew(t)[0] === target || !!el.selectNew(target).parents(t)[0]))
    && !(~(focusTags?.indexOf(tag) ?? -1) && !el.selectNew(target).extendUtils($pseudo).is(":focus"));
}
/* -------------------- */

/* sets content wrapper scrollTop/scrollLeft always to 0 */
export function _wrapperScroll($this: JQueryFnsWrapper) {
  var d = $this.extendUtils($data).data(pluginPfx),
    wrapper = $this.selectNew(`#mCSB_${d.idx}_container`).parent().extendUtils($offset, $event);
  wrapper.bind(`scroll.${pluginPfx}_${d.idx}`, function (e) {
    if (wrapper.scrollTop() !== 0 || wrapper.scrollLeft() !== 0) {
      $this.selectNew(`.mCSB_${d.idx}_scrollbar`).extendUtils($css).css("visibility", "hidden"); /* hide scrollbar(s) */
    }
  });
}
/* -------------------- */


/* returns a yx array from value */
export function _arr($this: JQueryFnsWrapper, val: any): [number, number] {
  var o = $this.extendUtils($data).data(pluginPfx).opt, vals = [];
  if (typeof val === "function") { val = val(); } /* check if the value is a single anonymous function */
  /* check if value is object or array, its length and create an array with yx values */
  if (!(val instanceof Array)) { /* object value (e.g. {y:"100",x:"100"}, 100 etc.) */
    vals[0] = val.y ? val.y : val.x || o.axis === "x" ? null : val;
    vals[1] = val.x ? val.x : val.y || o.axis === "y" ? null : val;
  } else { /* array value (e.g. [100,100]) */
    vals = val.length > 1 ? [val[0], val[1]] : o.axis === "x" ? [null, val[0]] : [val[0], null];
  }
  /* check if array values are anonymous functions */
  if (typeof vals[0] === "function") { vals[0] = vals[0](); }
  if (typeof vals[1] === "function") { vals[1] = vals[1](); }
  return vals as any;
}
/* -------------------- */




/* snaps scrolling to a multiple of a pixels number */
export function _snapAmount(to: number, amount: number, offset: number) {
  return (Math.round(to / amount) * amount - offset);
}
/* -------------------- */


/* stops content and scrollbar animations */
export function _stop(el: JQueryFnsWrapper) {
  var d = el.extendUtils($data).data(pluginPfx),
    sel = el.selectNew(`#mCSB_${d.idx}_container,#mCSB_${d.idx}_container_wrapper,#mCSB_${d.idx}_dragger_vertical,#mCSB_${d.idx}_dragger_horizontal`);
  sel.each(function () {
    _stopTween(this);
  });
}
/* -------------------- */
/* returns current time */
export function _getTime() {
  if (window.performance && window.performance.now) {
    return window.performance.now();
  } else {
    if (window.performance && window.performance['webkitNow']) {
      return window.performance['webkitNow']();
    } else {
      if (Date.now) { return Date.now(); } else { return new Date().getTime(); }
    }
  }
}
/* -------------------- */


/* stops a tween */
export function _stopTween(el: HTMLElement) {
  const elTween = retrieveState('tween', el, () => ({ top: {}, left: {} }));

  ["top", "left"].map(prop => elTween[prop]).forEach(tween => {
    if (tween.id) {
      if (!window.requestAnimationFrame) {
        clearTimeout(tween.id);
      } else {
        window.cancelAnimationFrame(tween.id);
      }
      tween.id = null;
      tween.stop = 1;
    }
  });
}
/* -------------------- */


/* deletes a property (avoiding the exception thrown by IE) */
export function _delete(c, m) {
  try { delete c[m]; } catch (e) { c[m] = null; }
}
/* -------------------- */


/* detects left mouse button */
export function _mouseBtnLeft(e) {
  return !(e.which && e.which !== 1);
}
/* -------------------- */


/* detects if pointer type event is touch */
export function _pointerTouch(e) {
  var t = e.originalEvent.pointerType;
  return !(t && t !== "touch" && t !== 2);
}
/* -------------------- */


/* checks if value is numeric */
export function _isNumeric(val: any): val is number {
  return !isNaN(parseFloat(val)) && isFinite(val);
}
/* -------------------- */


/* returns element position according to content */
export function _childPos(elWrapper: JQueryFnsWrapper) {
  const el = elWrapper?.extendUtils($offset);
  const p = el?.parents(".mCSB_container");
  const pOffset = p?.offset() || { top: 0, left: 0 };
  const elOffset = el?.offset() || { top: 0, left: 0 };
  return [elOffset.top - pOffset.top, elOffset.left - pOffset.left];
}
/* -------------------- */


/* checks if browser tab is hidden/inactive via Page Visibility API */
export function _isTabHidden() {
  const _getHiddenProp = () => {
    var pfx = ["webkit", "moz", "ms", "o"];
    if ("hidden" in document) return "hidden"; //natively supported
    for (var i = 0; i < pfx.length; i++) { //prefixed
      if ((pfx[i] + "Hidden") in document)
        return pfx[i] + "Hidden";
    }
    return null; //not supported
  }

  var prop = _getHiddenProp();
  if (!prop) return false;
  return document[prop];
}
/* -------------------- */


export function _createEventQuery(namespace: string | number) {
  let ns = ((namespace ?? false) !== false && !isNaN(namespace as any)) ? `${pluginPfx}_${namespace}` : namespace;
  if (false !== (ns ?? false)) { ns = '.' + ns; }

  return (...eventTypes: string[]) => eventTypes.map(ev => `${ev}${ns}`).join(' ');
}
