import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $data } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { classMap, pluginPfx } from '../variables';
import { _scrollTo } from './animation';
import { _delete, _isNumeric, _stop } from './utils';

/* scrolls content sequentially (used when scrolling via buttons, keyboard arrows etc.) */
export function _sequentialScroll(el: JQueryFnsWrapper, action: string, trigger: string | number, e?, s?) {
  var d = el.extendUtils($data).data(pluginPfx), o = d.opt, seq = d.sequential,
    mCSB_container = el.selectNew(`#mCSB_${d.idx}_container`)[0],
    once = seq.type === "stepped" ? true : false,
    steplessSpeed = o.scrollInertia < 26 ? 26 : o.scrollInertia, /* 26/1.5=17 */
    steppedSpeed = o.scrollInertia < 1 ? 17 : o.scrollInertia;

  /* starts sequence */
  const _on = (once?: boolean) => {
    if (o.snapAmount) { seq.scrollAmount = !(o.snapAmount instanceof Array) ? o.snapAmount : seq.dir[0] === "x" ? o.snapAmount[1] : o.snapAmount[0]; } /* scrolling snapping */
    var c = seq.type !== "stepped", /* continuous scrolling */
      t = s ? s : !once ? 1000 / 60 : c ? steplessSpeed / 1.5 : steppedSpeed, /* timer */
      m = !once ? 2.5 : c ? 7.5 : 40, /* multiplier */
      contentPos = [Math.abs(mCSB_container.offsetTop), Math.abs(mCSB_container.offsetLeft)],
      ratio = [d.scrollRatio.y > 10 ? 10 : d.scrollRatio.y, d.scrollRatio.x > 10 ? 10 : d.scrollRatio.x],
      amount = seq.dir[0] === "x" ? contentPos[1] + (seq.dir[1] * (ratio[1] * m)) : contentPos[0] + (seq.dir[1] * (ratio[0] * m)),
      px = seq.dir[0] === "x" ? contentPos[1] + (seq.dir[1] * parseInt(seq.scrollAmount)) : contentPos[0] + (seq.dir[1] * parseInt(seq.scrollAmount)),
      to = seq.scrollAmount !== "auto" ? px : amount,
      easing = e ? e : !once ? "mcsLinear" : c ? "mcsLinearOut" : "mcsEaseInOut",
      onComplete = !once ? false : true;
    if (once && t < 17) {
      to = seq.dir[0] === "x" ? contentPos[1] : contentPos[0];
    }
    _scrollTo(el, to.toString(), { dir: seq.dir[0], scrollEasing: easing, dur: t, onComplete: onComplete });
    if (once) {
      seq.dir = false;
      return;
    }
    clearTimeout(seq.step);
    seq.step = setTimeout(function () {
      _on();
    }, t);
  }
  /* stops sequence */
  const _off = () => {
    clearTimeout(seq.step);
    _delete(seq, "step");
    _stop(el);
  }

  switch (action) {
    case "on":
      seq.dir = [
        [classMap.button.right, classMap.button.left, 39, 37].includes(trigger) ? "x" : "y",
        [classMap.button.up, classMap.button.left, 38, 37].includes(trigger) ? -1 : 1,
      ];
      _stop(el);
      if (_isNumeric(trigger) && seq.type === "stepped") { return; }
      _on(once);
      break;
    case "off":
      _off();
      if (once || (d.tweenRunning && seq.dir)) {
        _on(true);
      }
      break;
  }
}
/* -------------------- */
