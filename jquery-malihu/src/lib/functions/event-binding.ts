import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $data, $event } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _buttons } from '../events/button';
import { _draggerRail } from '../events/drag-rail';
import { _clearFocus, _focus } from '../events/focus';
import { _keyboard } from '../events/keyboard';
import { _mousewheel } from '../events/mouse-wheel';
import { _draggable } from '../events/scrollbar-drag';
import { _selectable } from '../events/select-text';
import { _contentDraggable } from '../events/touch-swipe';
import { classMap, pluginPfx } from '../variables';
import { _canAccessIFrame } from './iframe';
import { _delete, _wrapperScroll } from './utils';

/* binds scrollbar events */
export function _bindEvents($this: JQueryFnsWrapper) {
  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt;
  if (!d.bindEvents) { /* check if events are already bound */
    _draggable($this);
    if (o.contentTouchScroll) { _contentDraggable($this); }
    _selectable($this);
    if (o.mouseWheel.enable) {
      _mousewheel($this);
    }
    _draggerRail($this);
    _wrapperScroll($this);
    if (o.advanced.autoScrollOnFocus) { _focus($this); }
    if (o.scrollButtons.enable) { _buttons($this); }
    if (o.keyboard.enable) { _keyboard($this); }
    d.bindEvents = true;
  }
}
/* -------------------- */


/* unbinds scrollbar events */
export function _unbindEvents($this: JQueryFnsWrapper) {
  const $thisExtra = $this.extendUtils($event);

  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt,
    namespace = `${pluginPfx}_${d.idx}`,
    sb = `.mCSB_${d.idx}_scrollbar`;
  let sel = $thisExtra.selectNew([
    `#mCSB_${d.idx}`,
    `#mCSB_${d.idx}_container`,
    `#mCSB_${d.idx}_container_wrapper`,
    `${sb} .${classMap.dragger.container}`,
    `#mCSB_${d.idx}_dragger_vertical`,
    `#mCSB_${d.idx}_dragger_horizontal`,
    `${sb}>a`
  ].join(', '));


  if (o.advanced.releaseDraggableSelectors) { sel = sel.add(o.advanced.releaseDraggableSelectors); }
  if (o.advanced.extraDraggableSelectors) { sel = sel.add(o.advanced.extraDraggableSelectors); }
  if (d.bindEvents) { /* check if events are bound */
    /* unbind namespaced events from document/selectors */
    $thisExtra.selectDocument().add($thisExtra.selectNew(<any>!_canAccessIFrame() || top.document)).unbind("." + namespace);
    sel.each(function () {
      $thisExtra.selectNew(this).unbind("." + namespace);
    });
    /* clear and delete timeouts/objects */
    _clearFocus($this[0]);
    clearTimeout(d.sequential.step); _delete(d.sequential, "step");

    const mCSB_container = $thisExtra.selectNew(`#mCSB_${d.idx}_container`);
    if (mCSB_container.length) {
      clearTimeout(mCSB_container[0]['onCompleteTimeout']);
      _delete(mCSB_container[0], "onCompleteTimeout");
    }

    d.bindEvents = false;
  }
}
/* -------------------- */
