import { MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { classMap, pluginPfx } from '../variables';
import { _delete } from './utils';


export enum MalihuUpdateCallback {
  UPDATE = 1,
  IMAGE_LOAD = 2,
  SELECTOR_CHANGE = 3,
}

export function _removeAutoUpdate(el: JQueryFnsWrapper) {
  if (el) {
    const d = el.extendUtils($data).data(pluginPfx);
    const mCSB_container = el.selectNew(`#mCSB_${d.idx}_container`);

    if (mCSB_container.length) {
      clearTimeout(mCSB_container[0]['autoUpdate']);
      _delete(mCSB_container[0], "autoUpdate");
    }
  }
}


/* calls the update method automatically */
export function _autoUpdate($this: JQueryFnsWrapper, invoker: (cb: MalihuUpdateCallback) => void) {
  var d = $this.extendUtils($data).data(pluginPfx), o: MalihuScrollbarOptions = d.opt,
    mCSB_container = $this.selectNew(`#mCSB_${d.idx}_container`);

  const upd = () => {
    clearTimeout(mCSB_container[0]['autoUpdate']);
    if ($this.parents("html").length === 0) {
      /* check element in dom tree */
      $this = null;
      return;
    }
    mCSB_container[0]['autoUpdate'] = setTimeout(function () {
      /* update on specific selector(s) length and size change */
      if (o.advanced.updateOnSelectorChange) {
        d.poll.change.n = sizesSum();
        if (d.poll.change.n !== d.poll.change.o) {
          d.poll.change.o = d.poll.change.n;
          doUpd(MalihuUpdateCallback.SELECTOR_CHANGE);
          return;
        }
      }
      /* update on main element and scrollbar size changes */
      if (o.advanced.updateOnContentResize) {
        d.poll.size.n = $this[0].scrollHeight + $this[0].scrollWidth + mCSB_container[0].offsetHeight + $this[0].offsetHeight + $this[0].offsetWidth;
        if (d.poll.size.n !== d.poll.size.o) {
          d.poll.size.o = d.poll.size.n;
          doUpd(MalihuUpdateCallback.UPDATE);
          return;
        }
      }
      /* update on image load */
      if (o.advanced.updateOnImageLoad) {
        if (!(o.advanced.updateOnImageLoad === "auto" && o.axis === "y")) { //by default, it doesn't run on vertical content
          d.poll.img.n = mCSB_container.find("img").length;
          if (d.poll.img.n !== d.poll.img.o) {
            d.poll.img.o = d.poll.img.n;
            mCSB_container.find("img").each(function () {
              imgLoader(<HTMLImageElement>this);
            });
            return;
          }
        }
      }
      if (o.advanced.updateOnSelectorChange || o.advanced.updateOnContentResize || o.advanced.updateOnImageLoad) { upd(); }
    }, o.advanced.autoUpdateTimeout);
  }

  const $thisCss = $this.extendUtils($css);
  /* a tiny image loader */
  const imgLoader = (el: HTMLImageElement) => {
    if ($thisCss.selectNew(el).hasClass(classMap.img.loaded)) { doUpd(); return; }
    var img = new Image();
    const createDelegate = (contextObject, delegateMethod) => {
      return (...args) => { return delegateMethod.apply(contextObject, ...args); }
    }
    function imgOnLoad() {
      this.onload = null;
      $thisCss.selectNew(el).addClass(classMap.img.loaded);
      doUpd(MalihuUpdateCallback.IMAGE_LOAD);
    }
    img.onload = createDelegate(img, imgOnLoad);
    img.src = el.src;
  }
  /* returns the total height and width sum of all elements matching the selector */
  const sizesSum = () => {
    if (o.advanced.updateOnSelectorChange === true) { o.advanced.updateOnSelectorChange = "*"; }
    var total = 0, sel = (selEl => selEl && mCSB_container.find(selEl))(o.advanced.updateOnSelectorChange);
    if (o.advanced.updateOnSelectorChange && sel?.length > 0) {
      sel.each(function () {
        total += this.offsetHeight + this.offsetWidth;
      });
    }
    return total;
  }
  /* calls the update method */
  const doUpd = (cb?: MalihuUpdateCallback) => {
    clearTimeout(mCSB_container[0]['autoUpdate']);
    invoker(cb);
  }

  upd();
}
/* -------------------- */
