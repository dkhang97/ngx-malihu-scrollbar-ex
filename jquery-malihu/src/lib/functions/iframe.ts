import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';


/* checks if iframe can be accessed */
export const _canAccessIFrameCache: Record<string, boolean> = {};
export function _canAccessIFrame(iframe?: HTMLIFrameElement) {
  let result = false, cacheKey: boolean | string = false, html: string = null;
  if (iframe === undefined) {
    cacheKey = "#empty";
  } else if (iframe.getAttribute('id') !== undefined) {
    cacheKey = iframe.getAttribute('id');
  }
  if (cacheKey !== false && _canAccessIFrameCache[cacheKey] !== undefined) {
    return _canAccessIFrameCache[cacheKey];
  }
  if (!iframe) {
    try {
      var doc = top.document;
      html = doc.body.innerHTML;
    } catch (err) {/* do nothing */ }
    result = (html !== null);
  } else {
    try {
      var frameDoc = iframe.contentDocument || iframe.contentWindow.document;
      html = frameDoc.body.innerHTML;
    } catch (err) {/* do nothing */ }
    result = (html !== null);
  }
  if (cacheKey !== false) { _canAccessIFrameCache[cacheKey] = result; }
  return result;
}
/* -------------------- */


/* switches iframe's pointer-events property (drag, mousewheel etc. over cross-domain iframes) */
export function _iframe($this: JQueryFnsWrapper, evt: boolean) {
  var el = $this.find("iframe");
  if (!el.length) { return; } /* check if content contains iframes */
  var val = !evt ? "none" : "auto";
  el.extendUtils($css).css("pointer-events", val); /* for IE11, iframe's display property should not be "block" */
}
/* -------------------- */
