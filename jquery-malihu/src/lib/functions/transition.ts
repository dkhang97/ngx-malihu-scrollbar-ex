import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { pluginPfx } from '../variables';
import { _scrollTo } from './animation';
import { _stop } from './utils';


/* resets content position to 0 */
export function _resetContentPosition($this: JQueryFnsWrapper) {
  const $w = $create($this.document, $dimensions, $css);

  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt,
    mCustomScrollBox = $w(`#mCSB_${d.idx}`),
    mCSB_container = $w(`#mCSB_${d.idx}_container`),
    [verticalDragger, horizontalDragger] =
      [$w(`#mCSB_${d.idx}_dragger_vertical`), $w(`#mCSB_${d.idx}_dragger_horizontal`)];
  _stop($this); /* stop any current scrolling before resetting */
  if ((o.axis !== "x" && !d.overflowed[0]) || (o.axis === "y" && d.overflowed[0])) { /* reset y */
    verticalDragger.add(mCSB_container);
    verticalDragger.css("top", 0);
    _scrollTo($this, "_resetY");
  }
  if ((o.axis !== "y" && !d.overflowed[1]) || (o.axis === "x" && d.overflowed[1])) { /* reset x */
    let cx = 0, dx = 0;
    if (d.langDir === "rtl") { /* adjust left position for rtl direction */
      cx = mCustomScrollBox.width() - mCSB_container.outerWidth(false);
      dx = Math.abs(cx / d.scrollRatio.x);
    }
    mCSB_container.css("left", cx);
    horizontalDragger.css("left", dx);
    _scrollTo($this, "_resetX");
  }
}
/* -------------------- */
