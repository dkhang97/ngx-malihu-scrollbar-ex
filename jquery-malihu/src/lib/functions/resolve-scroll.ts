import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import update from '../methods/update';
import { pluginPfx } from '../variables';
import { _childPos, _isNumeric } from './utils';

/* translates values (e.g. "top", 100, "100px", "#id") to actual scroll-to positions */
export function _resolveScrollTo($this: JQueryFnsWrapper, val: any, dir: string) {
  if (val == null || typeof val == "undefined") { return; }
  const $w = $create($this.document, $data, $dimensions, $css);

  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt,
    mCSB_container = $w(`#mCSB_${d.idx}_container`),
    wrapper = mCSB_container.parent(),
    t = typeof val;
  if (!dir) { dir = o.axis === "x" ? "x" : "y"; }
  const contentLength = dir === "x" ? mCSB_container.outerWidth(false) - wrapper.width() : mCSB_container.outerHeight(false) - wrapper.height(),
    contentPos = dir === "x" ? mCSB_container[0].offsetLeft : mCSB_container[0].offsetTop,
    cssProp = dir === "x" ? "left" : "top";
  switch (t) {
    case "function": /* this currently is not used. Consider removing it */
      return val();
    case "object": /* js/jquery object */
      const obj = val.jquery ? val : $w(val);
      if (!obj.length) { return; }
      return dir === "x" ? _childPos(obj)[1] : _childPos(obj)[0];
    case "string": case "number":
      if (_isNumeric(val)) { /* numeric value */
        return Math.abs(val);
      } else if (!val.indexOf("%")) { /* percentage value */
        return Math.abs(contentLength * parseInt(val) / 100);
      } else if (!val.indexOf("-=")) { /* decrease value */
        return Math.abs(contentPos - parseInt(val.split("-=")[1]));
      } else if (!val.indexOf("+=")) { /* inrease value */
        const p = (contentPos + parseInt(val.split("+=")[1]));
        return p >= 0 ? 0 : Math.abs(p);
      } else if (!val.indexOf("px") && _isNumeric(val.split("px")[0])) { /* pixels string value (e.g. "100px") */
        return Math.abs(val.split("px")[0]);
      } else {
        if (val === "top" || val === "left") { /* special strings */
          return 0;
        } else if (val === "bottom") {
          return Math.abs(wrapper.height() - mCSB_container.outerHeight(false));
        } else if (val === "right") {
          return Math.abs(wrapper.width() - mCSB_container.outerWidth(false));
        } else if (val === "first" || val === "last") {
          const flObj = mCSB_container.find(":" + val);
          return dir === "x" ? _childPos(flObj)[1] : _childPos(flObj)[0];
        } else {
          const $val = $w(val);
          if ($val.length) { /* jquery selector */
            return dir === "x" ? _childPos($val)[1] : _childPos($val)[0];
          } else { /* other values (e.g. "100em") */
            mCSB_container.css(cssProp, val);
            update($this);
            return;
          }
        }
      }
      break;
  }
}
/* -------------------- */
