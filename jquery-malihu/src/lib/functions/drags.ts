import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { classMap, oldIE, pluginPfx } from '../variables';




/* auto-adjusts scrollbar dragger length */
export function _setDraggerLength($this: JQueryFnsWrapper) {
  const $w = $create($this.document, $dimensions, $css);

  var d = $this.extendUtils($data).data(pluginPfx),
    mCustomScrollBox = $w(`#mCSB_${d.idx}`),
    mCSB_container = $w(`#mCSB_${d.idx}_container`),
    mCSB_dragger = [$w(`#mCSB_${d.idx}_dragger_vertical`), $w(`#mCSB_${d.idx}_dragger_horizontal`)],
    ratio = [mCustomScrollBox.height() / mCSB_container.outerHeight(false), mCustomScrollBox.width() / mCSB_container.outerWidth(false)],
    l = [
      parseInt(mCSB_dragger[0].css("min-height")), Math.round(ratio[0] * mCSB_dragger[0].parent().height()),
      parseInt(mCSB_dragger[1].css("min-width")), Math.round(ratio[1] * mCSB_dragger[1].parent().width())
    ],
    h = oldIE && (l[1] < l[0]) ? l[0] : l[1], w = oldIE && (l[3] < l[2]) ? l[2] : l[3];
  mCSB_dragger[0].css({
    "height": h, "max-height": (mCSB_dragger[0].parent().height() - 10)
  });
  mCSB_dragger[0].find(".mCSB_dragger_bar").css({ "line-height": l[0] + "px" });
  mCSB_dragger[1].css({
    "width": w, "max-width": (mCSB_dragger[1].parent().width() - 10)
  });
}
/* -------------------- */


/* calculates scrollbar to content ratio */
export function _scrollRatio($this: JQueryFnsWrapper) {
  const $w = $create($this.document, $dimensions);

  var d = $this.extendUtils($data).data(pluginPfx),
    mCustomScrollBox = $w(`#mCSB_${d.idx}`),
    mCSB_container = $w(`#mCSB_${d.idx}_container`),
    mCSB_dragger = [$w(`#mCSB_${d.idx}_dragger_vertical`), $w(`#mCSB_${d.idx}_dragger_horizontal`)],
    [scrollAmountHeight, scrollAmountWidth] = [mCSB_container.outerHeight(false) - mCustomScrollBox.height(), mCSB_container.outerWidth(false) - mCustomScrollBox.width()],
    [ratioHeight, ratioWidth] = [
      scrollAmountHeight / (mCSB_dragger[0].parent().height() - mCSB_dragger[0].height()),
      scrollAmountWidth / (mCSB_dragger[1].parent().width() - mCSB_dragger[1].width())
    ];
  d.scrollRatio = { y: ratioHeight, x: ratioWidth };
}
/* -------------------- */


/* toggles scrolling classes */
export function _onDragClasses(orgEl: JQueryFnsWrapper, action?: string, autoExpand?: boolean) {
  const el = orgEl.extendUtils($css);

  var expandClass = autoExpand ? `${classMap.dragger.onDrag}_expanded` : "",
    scrollbar = el.closest(".mCSB_scrollTools");
  if (action === "active") {
    el.toggleClass(`${classMap.dragger.onDrag} ${expandClass}`); scrollbar.toggleClass(classMap.scrollTools.onDrag);
    el[0]['_draggable'] = el[0]['_draggable'] ? 0 : 1;
  } else {
    if (!el[0]['_draggable']) {
      if (action === "hide") {
        el.removeClass(classMap.dragger.onDrag); scrollbar.removeClass(classMap.scrollTools.onDrag);
      } else {
        el.addClass(classMap.dragger.onDrag); scrollbar.addClass(classMap.scrollTools.onDrag);
      }
    }
  }
}
/* -------------------- */
