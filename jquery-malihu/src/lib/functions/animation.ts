import { MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { mergeObject } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { $css, $data, $dimensions, $offset } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { retrieveState } from '../state';
import { pluginPfx } from '../variables';
import { _onDragClasses } from './drags';
import { _arr, _getTime, _snapAmount } from './utils';


interface ScrollToOption {
  trigger?: string;
  dir?: string;
  scrollEasing?: string;
  drag?: boolean;
  dur?: number;
  overwrite?: string;
  callbacks?: boolean
  onStart?: boolean;
  onUpdate?: boolean;
  onComplete?: boolean;
}

/*
ANIMATES CONTENT
This is where the actual scrolling happens
*/
export function _scrollTo(elFns: JQueryFnsWrapper, to, options?: ScrollToOption) {
  const el = elFns.extendUtils($data).extendUtils($dimensions, $css);

  const d = el.data(pluginPfx), o: MalihuScrollbarOptions = d?.opt,
    defaults = {
      trigger: "internal",
      dir: "y",
      scrollEasing: "mcsEaseOut",
      drag: false,
      dur: o.scrollInertia,
      overwrite: "all",
      callbacks: true,
      onStart: true,
      onUpdate: true,
      onComplete: true
    };

  if (!o) { return; }

  var options = <ScrollToOption>mergeObject(defaults, options),
    dur = [options.dur, (options.drag ? 0 : options.dur)],
    mCustomScrollBox = el.selectNew(`#mCSB_${d.idx}`),
    mCSB_container = elFns.extendUtils($dimensions).selectNew(`#mCSB_${d.idx}_container`),
    wrapper = mCSB_container.parent().extendUtils($offset),
    totalScrollOffsets = o.callbacks.onTotalScrollOffset ? _arr(el, o.callbacks.onTotalScrollOffset) : [0, 0],
    totalScrollBackOffsets = o.callbacks.onTotalScrollBackOffset ? _arr(el, o.callbacks.onTotalScrollBackOffset) : [0, 0];
  d.trigger = options.trigger;
  if (wrapper.scrollTop() !== 0 || wrapper.scrollLeft() !== 0) { /* always reset scrollTop/Left */
    el.selectNew(`.mCSB_${d.idx}_scrollbar`).css("visibility", "visible");
    wrapper.scrollTop(0);
    wrapper.scrollLeft(0);
  }
  /* checks if callback function exists */
  const _cb = cb => d && o.callbacks[cb] && typeof o.callbacks[cb] === "function";

  /* checks whether callback offsets always trigger */
  const _cbOffsets = () => [o.callbacks.alwaysTriggerOffsets || contentPos >= limit[0] + tso, o.callbacks.alwaysTriggerOffsets || contentPos <= -tsbo];

  /*
  populates object with useful values for the user
  values:
      content: this.mcs.content
      content top position: this.mcs.top
      content left position: this.mcs.left
      dragger top position: this.mcs.draggerTop
      dragger left position: this.mcs.draggerLeft
      scrolling y percentage: this.mcs.topPct
      scrolling x percentage: this.mcs.leftPct
      scrolling direction: this.mcs.direction
  */
  const _mcs = () => {
    var cp = [mCSB_container[0].offsetTop, mCSB_container[0].offsetLeft], /* content position */
      dp = [mCSB_dragger[0].offsetTop, mCSB_dragger[0].offsetLeft], /* dragger position */
      cl = [mCSB_container.outerHeight(false), mCSB_container.outerWidth(false)], /* content length */
      pl = [mCustomScrollBox.height(), mCustomScrollBox.width()]; /* content parent length */
    el[0]['mcs'] = {
      content: mCSB_container, /* original content wrapper as jquery object */
      top: cp[0], left: cp[1], draggerTop: dp[0], draggerLeft: dp[1],
      topPct: Math.round((100 * Math.abs(cp[0])) / (Math.abs(cl[0]) - pl[0])), leftPct: Math.round((100 * Math.abs(cp[1])) / (Math.abs(cl[1]) - pl[1])),
      direction: options.dir
    };
    /*
    this refers to the original element containing the scrollbar(s)
    usage: this.mcs.top, this.mcs.leftPct etc.
    */
  }

  if (to === "_resetY" && !d.contentReset.y) {
    /* callbacks: onOverflowYNone */
    if (_cb("onOverflowYNone")) { o.callbacks.onOverflowYNone.call(el[0]); }
    d.contentReset.y = 1;
  }
  if (to === "_resetX" && !d.contentReset.x) {
    /* callbacks: onOverflowXNone */
    if (_cb("onOverflowXNone")) { o.callbacks.onOverflowXNone.call(el[0]); }
    d.contentReset.x = 1;
  }
  if (to === "_resetY" || to === "_resetX") { return; }
  if ((d.contentReset.y || !el[0]['mcs']) && d.overflowed[0]) {
    /* callbacks: onOverflowY */
    if (_cb("onOverflowY")) { o.callbacks.onOverflowY.call(el[0]); }
    d.contentReset.y = null;
  }
  if ((d.contentReset.x || !el[0]['mcs']) && d.overflowed[1]) {
    /* callbacks: onOverflowX */
    if (_cb("onOverflowX")) { o.callbacks.onOverflowX.call(el[0]); }
    d.contentReset.x = null;
  }
  if (o.snapAmount) { /* scrolling snapping */
    var snapAmount = !(o.snapAmount instanceof Array) ? o.snapAmount : options.dir === "x" ? o.snapAmount[1] : o.snapAmount[0];
    to = _snapAmount(to, snapAmount, o.snapOffset);
  }
  switch (options.dir) {
    case "x":
      var mCSB_dragger = el.selectNew(`#mCSB_${d.idx}_dragger_horizontal`),
        property = "left",
        contentPos = mCSB_container[0].offsetLeft,
        limit = [
          mCustomScrollBox.width() - mCSB_container.outerWidth(false),
          mCSB_dragger.parent().width() - mCSB_dragger.width()
        ],
        scrollTo = [to, to === 0 ? 0 : (to / d.scrollRatio.x)],
        tso = totalScrollOffsets[1],
        tsbo = totalScrollBackOffsets[1],
        totalScrollOffset = tso > 0 ? tso / d.scrollRatio.x : 0,
        totalScrollBackOffset = tsbo > 0 ? tsbo / d.scrollRatio.x : 0;
      break;
    case "y":
      var mCSB_dragger = el.selectNew(`#mCSB_${d.idx}_dragger_vertical`),
        property = "top",
        contentPos = mCSB_container[0].offsetTop,
        limit = [
          mCustomScrollBox.height() - mCSB_container.outerHeight(false),
          mCSB_dragger.parent().height() - mCSB_dragger.height()
        ],
        scrollTo = [to, to === 0 ? 0 : (to / d.scrollRatio.y)],
        tso = totalScrollOffsets[0],
        tsbo = totalScrollBackOffsets[0],
        totalScrollOffset = tso > 0 ? tso / d.scrollRatio.y : 0,
        totalScrollBackOffset = tsbo > 0 ? tsbo / d.scrollRatio.y : 0;
      break;
  }
  if (scrollTo[1] < 0 || (scrollTo[0] === 0 && scrollTo[1] === 0)) {
    scrollTo = [0, 0];
  } else if (scrollTo[1] >= limit[1]) {
    scrollTo = [limit[0], limit[1]];
  } else {
    scrollTo[0] = -scrollTo[0];
  }
  if (!el[0]['mcs']) {
    _mcs();  /* init mcs object (once) to make it available before callbacks */
    if (_cb("onInit")) { o.callbacks.onInit.call(el[0]); } /* callbacks: onInit */
  }
  clearTimeout(mCSB_container[0]['onCompleteTimeout']);
  _tweenTo(mCSB_dragger[0], property, Math.round(scrollTo[1]), dur[1], options.scrollEasing);
  if (!d.tweenRunning && ((contentPos === 0 && scrollTo[0] >= 0) || (contentPos === limit[0] && scrollTo[0] <= limit[0]))) { return; }
  _tweenTo(mCSB_container[0], property, Math.round(scrollTo[0]), dur[0], options.scrollEasing, options.overwrite, {
    onStart() {
      if (options.callbacks && options.onStart && !d.tweenRunning) {
        /* callbacks: onScrollStart */
        if (_cb("onScrollStart")) { _mcs(); o.callbacks.onScrollStart.call(el[0]); }
        d.tweenRunning = true;
        _onDragClasses(mCSB_dragger);
        d.cbOffsets = _cbOffsets();
      }
    },
    onUpdate() {
      if (options.callbacks && options.onUpdate) {
        /* callbacks: whileScrolling */
        if (_cb("whileScrolling")) { _mcs(); o.callbacks.whileScrolling.call(el[0]); }
      }
    },
    onComplete() {
      if (options.callbacks && options.onComplete) {
        if (o.axis === "yx") { clearTimeout(mCSB_container[0]['onCompleteTimeout']); }
        var t = mCSB_container[0]['idleTimer'] || 0;
        mCSB_container[0]['onCompleteTimeout'] = <any>setTimeout(function () {
          /* callbacks: onScroll, onTotalScroll, onTotalScrollBack */
          if (_cb("onScroll")) { _mcs(); o.callbacks.onScroll.call(el[0]); }
          if (_cb("onTotalScroll") && scrollTo[1] >= limit[1] - totalScrollOffset && d.cbOffsets[0]) { _mcs(); o.callbacks.onTotalScroll.call(el[0]); }
          if (_cb("onTotalScrollBack") && scrollTo[1] <= totalScrollBackOffset && d.cbOffsets[1]) { _mcs(); o.callbacks.onTotalScrollBack.call(el[0]); }
          d.tweenRunning = false;
          mCSB_container[0]['idleTimer'] = 0;
          _onDragClasses(mCSB_dragger, "hide");
        }, t);
      }
    }
  });
}
/* -------------------- */


/*
CUSTOM JAVASCRIPT ANIMATION TWEEN
Lighter and faster than jquery animate() and css transitions
Animates top/left properties and includes easings
*/
export function _tweenTo(el: HTMLElement, prop: string, to: number, duration: number, easing: string, overwrite?: string, callbacks?: {
  onStart?(): void, onUpdate?(): void, onComplete?(): void,
}) {
  const elTween = retrieveState('tween', el, () => ({ top: {}, left: {} }));

  const startTime = _getTime(), elStyle = el.style, tobj = elTween[prop];
  let progress = 0, from = el.offsetTop, _delay: number;
  let _request: (callback: FrameRequestCallback) => number;

  if (prop === "left") { from = el.offsetLeft; }
  var diff = to - from;
  tobj.stop = 0;
  const _step = () => {
    if (tobj.stop) { return; }
    if (!progress) { callbacks?.onStart?.(); }
    progress = _getTime() - startTime;
    _tween();
    if (progress >= tobj.time) {
      tobj.time = (progress > tobj.time) ? progress + _delay - (progress - tobj.time) : progress + _delay - 1;
      if (tobj.time < progress + 1) { tobj.time = progress + 1; }
    }
    if (tobj.time < duration) { tobj.id = _request(_step); } else { callbacks?.onComplete?.(); }
  }
  const _tween = () => {
    if (duration > 0) {
      tobj.currVal = _ease(tobj.time, from, diff, duration, easing);
      elStyle[prop] = Math.round(tobj.currVal) + "px";
    } else {
      elStyle[prop] = to + "px";
    }
    callbacks?.onUpdate?.();
  }
  const _startTween = () => {
    _delay = 1000 / 60;
    tobj.time = progress + _delay;
    _request = (!window.requestAnimationFrame) ? (f => { _tween(); return setTimeout(f, 0.01) as any; }) : window.requestAnimationFrame;
    tobj.id = _request(_step);
  }
  const _cancelTween = () => {
    if (tobj.id == null) { return; }
    if (!window.requestAnimationFrame) {
      clearTimeout(tobj.id);
    } else { window.cancelAnimationFrame(tobj.id); }
    tobj.id = null;
  }
  const _ease = (t, b, c, d, type) => {
    switch (type) {
      case "linear": case "mcsLinear":
        return c * t / d + b;
      case "mcsLinearOut":
        t /= d; t--; return c * Math.sqrt(1 - t * t) + b;
      case "easeInOutSmooth":
        t /= d / 2;
        if (t < 1) return c / 2 * t * t + b;
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
      case "easeInOutStrong":
        t /= d / 2;
        if (t < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        t--;
        return c / 2 * (-Math.pow(2, -10 * t) + 2) + b;
      case "easeInOut": case "mcsEaseInOut":
        t /= d / 2;
        if (t < 1) return c / 2 * t * t * t + b;
        t -= 2;
        return c / 2 * (t * t * t + 2) + b;
      case "easeOutSmooth":
        t /= d; t--;
        return -c * (t * t * t * t - 1) + b;
      case "easeOutStrong":
        return c * (-Math.pow(2, -10 * t / d) + 1) + b;
      case "easeOut": case "mcsEaseOut": default:
        var ts = (t /= d) * t, tc = ts * t;
        return b + c * (0.499999999999997 * tc * ts + -2.5 * ts * ts + 5.5 * tc + -6.5 * ts + 4 * t);
    }
  }
  if (overwrite !== "none") { _cancelTween(); }
  _startTween();
}
		/* -------------------- */
