import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $data, $event } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _sequentialScroll } from '../functions/sequential-scroll';
import { _createEventQuery, _mouseBtnLeft } from '../functions/utils';
import { pluginPfx, updateTouchActive } from '../variables';

/*
BUTTONS EVENTS
scrolls content via up, down, left and right buttons
*/
export function _buttons($this: JQueryFnsWrapper) {
  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt, seq = d.sequential,
    sel = `.mCSB_${d.idx}_scrollbar`,
    btn = $this.extendUtils($event).selectNew(`${sel}>a`);

  const eventQuery = _createEventQuery(d.idx);

  btn.bind(eventQuery('contextmenu'), function (e) {
    e.preventDefault(); //prevent right click
  });
  btn.bind(eventQuery('mousedown', 'touchstart', 'pointerdown', 'MSPointerDown', 'mouseup', 'touchend', 'pointerup', 'MSPointerUp', 'mouseout', 'pointerout', 'MSPointerOut', 'click'), function (e) {
    e.preventDefault();
    if (!_mouseBtnLeft(e)) { return; } /* left mouse button only */
    const btnClass = $this[0]?.getAttribute('class');
    seq.type = o.scrollButtons.scrollType;

    const _seq = (a: string, c) => {
      seq.scrollAmount = o.scrollButtons.scrollAmount;
      _sequentialScroll($this, a, c);
    }

    switch (e.type) {
      case "mousedown": case "touchstart": case "pointerdown": case "MSPointerDown":
        if (seq.type === "stepped") { return; }
        updateTouchActive(true);
        d.tweenRunning = false;
        _seq("on", btnClass);
        break;
      case "mouseup": case "touchend": case "pointerup": case "MSPointerUp":
      case "mouseout": case "pointerout": case "MSPointerOut":
        if (seq.type === "stepped") { return; }
        updateTouchActive(false);
        if (seq.dir) { _seq("off", btnClass); }
        break;
      case "click":
        if (seq.type !== "stepped" || d.tweenRunning) { return; }
        _seq("on", btnClass);
        break;
    }
  });
}
/* -------------------- */
