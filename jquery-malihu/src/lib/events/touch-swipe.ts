import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions, $event, $offset } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _canAccessIFrame } from '../functions/iframe';
import { _coordinates, _createEventQuery, _getTime, _pointerTouch, _stop } from '../functions/utils';
import { pluginPfx, touchActive, updateTouchable } from '../variables';

/*
TOUCH SWIPE EVENTS
scrolls content via touch swipe
Emulates the native touch-swipe scrolling with momentum found in iOS, Android and WP devices
*/
export function _contentDraggable($this: JQueryFnsWrapper) {
  const $thisExtra = $this.extendUtils($css, $event, $offset, $dimensions);

  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt,
    mCustomScrollBox = $thisExtra.selectNew(`#mCSB_${d.idx}`),
    mCSB_container = $thisExtra.selectNew(`#mCSB_${d.idx}_container`),
    mCSB_dragger = [$thisExtra.selectNew(`#mCSB_${d.idx}_dragger_vertical`), $thisExtra.selectNew(`#mCSB_${d.idx}_dragger_horizontal`)],
    draggable, dragY, dragX, touchStartY, touchStartX, touchMoveY = [], touchMoveX = [], startTime, runningTime, endTime, distance, speed, amount,
    durA = 0, durB, overwrite = o.axis === "yx" ? "none" : "all", touchIntent = [], touchDrag, docDrag;

  const eventQuery = _createEventQuery(d.idx);

  const body = $thisExtra.document?.body;

  const iframe = mCSB_container.find("iframe"),
    events = [
      eventQuery('touchstart', 'pointerdown', 'MSPointerDown'), //start
      eventQuery('touchmove', 'pointermove', 'MSPointerMove'), //move
      eventQuery('touchend', 'pointerup', 'MSPointerUp'), //end
    ],
    touchAction = body?.style.touchAction !== undefined && body?.style.touchAction !== "";
  mCSB_container.bind(events[0], function (e) {
    _onTouchstart(e);
  });
  mCSB_container.bind(events[1], function (e) {
    _onTouchmove(e);
  });
  mCustomScrollBox.bind(events[0], function (e) {
    _onTouchstart2(e);
  });
  mCustomScrollBox.bind(events[2], function (e) {
    _onTouchend(e);
  });
  if (iframe.length) {
    iframe.each(function () {
      $thisExtra.selectNew(this).bind("load", function () {
        /* bind events on accessible iframes */
        if (_canAccessIFrame(<HTMLIFrameElement>this)) {
          const $frameContent = $thisExtra.selectNew((<HTMLIFrameElement>this).contentDocument || (<HTMLIFrameElement>this).contentWindow.document as any);
          $frameContent.bind(events[0], function (e) {
            _onTouchstart(e);
            _onTouchstart2(e);
          });
          $frameContent.bind(events[1], function (e) {
            _onTouchmove(e);
          });
          $frameContent.bind(events[2], function (e) {
            _onTouchend(e);
          });
        }
      });
    });
  }
  const _onTouchstart = e => {
    if (!_pointerTouch(e) || touchActive || _coordinates($this.document, e)[2]) { updateTouchable(false); return; }
    updateTouchable(true); touchDrag = 0; docDrag = 0; draggable = 1;
    $thisExtra.removeClass("mCS_touch_action");
    var offset = mCSB_container.offset();
    dragY = _coordinates($this.document, e)[0] - offset.top;
    dragX = _coordinates($this.document, e)[1] - offset.left;
    touchIntent = [_coordinates($this.document, e)[0], _coordinates($this.document, e)[1]];
  }
  const _onTouchmove = e => {
    if (!_pointerTouch(e) || touchActive || _coordinates($this.document, e)[2]) { return; }
    if (!o.documentTouchScroll) { e.preventDefault(); }
    e.stopImmediatePropagation();
    if (docDrag && !touchDrag) { return; }
    if (draggable) {
      runningTime = _getTime();
      var offset = mCustomScrollBox.offset(), y = _coordinates($this.document, e)[0] - offset.top, x = _coordinates($this.document, e)[1] - offset.left,
        easing = "mcsLinearOut";
      touchMoveY.push(y);
      touchMoveX.push(x);
      touchIntent[2] = Math.abs(_coordinates($this.document, e)[0] - touchIntent[0]); touchIntent[3] = Math.abs(_coordinates($this.document, e)[1] - touchIntent[1]);
      if (d.overflowed[0]) {
        var limit = mCSB_dragger[0].parent().height() - mCSB_dragger[0].height(),
          prevent = ((dragY - y) > 0 && (y - dragY) > -(limit * d.scrollRatio.y) && (touchIntent[3] * 2 < touchIntent[2] || o.axis === "yx"));
      }
      if (d.overflowed[1]) {
        var limitX = mCSB_dragger[1].parent().width() - mCSB_dragger[1].width(),
          preventX = ((dragX - x) > 0 && (x - dragX) > -(limitX * d.scrollRatio.x) && (touchIntent[2] * 2 < touchIntent[3] || o.axis === "yx"));
      }
      if (prevent || preventX) { /* prevent native document scrolling */
        if (!touchAction) { e.preventDefault(); }
        touchDrag = 1;
      } else {
        docDrag = 1;
        $thisExtra.addClass("mCS_touch_action");
      }
      if (touchAction) { e.preventDefault(); }
      amount = o.axis === "yx" ? [(dragY - y), (dragX - x)] : o.axis === "x" ? [null, (dragX - x)] : [(dragY - y), null];
      mCSB_container[0]['idleTimer'] = 250;
      if (d.overflowed[0]) { _drag(amount[0], durA, easing, "y", "all", true); }
      if (d.overflowed[1]) { _drag(amount[1], durA, easing, "x", overwrite, true); }
    }
  }
  const _onTouchstart2 = e => {
    if (!_pointerTouch(e) || touchActive || _coordinates($this.document, e)[2]) { updateTouchable(false); return; }
    updateTouchable(true);
    e.stopImmediatePropagation();
    _stop($this);
    startTime = _getTime();
    var offset = mCustomScrollBox.offset();
    touchStartY = _coordinates($this.document, e)[0] - offset.top;
    touchStartX = _coordinates($this.document, e)[1] - offset.left;
    touchMoveY = []; touchMoveX = [];
  }
  const _onTouchend = e => {
    if (!_pointerTouch(e) || touchActive || _coordinates($this.document, e)[2]) { return; }
    draggable = 0;
    e.stopImmediatePropagation();
    touchDrag = 0; docDrag = 0;
    endTime = _getTime();
    var offset = mCustomScrollBox.offset(), y = _coordinates($this.document, e)[0] - offset.top, x = _coordinates($this.document, e)[1] - offset.left;
    if ((endTime - runningTime) > 30) { return; }
    speed = 1000 / (endTime - startTime);
    var easing = "mcsEaseOut", slow = speed < 2.5,
      diff = slow ? [touchMoveY[touchMoveY.length - 2], touchMoveX[touchMoveX.length - 2]] : [0, 0];
    distance = slow ? [(y - diff[0]), (x - diff[1])] : [y - touchStartY, x - touchStartX];
    var absDistance = [Math.abs(distance[0]), Math.abs(distance[1])];
    speed = slow ? [Math.abs(distance[0] / 4), Math.abs(distance[1] / 4)] : [speed, speed];
    var a = [
      Math.abs(mCSB_container[0].offsetTop) - (distance[0] * _m((absDistance[0] / speed[0]), speed[0])),
      Math.abs(mCSB_container[0].offsetLeft) - (distance[1] * _m((absDistance[1] / speed[1]), speed[1]))
    ];
    amount = o.axis === "yx" ? [a[0], a[1]] : o.axis === "x" ? [null, a[1]] : [a[0], null];
    durB = [(absDistance[0] * 4) + o.scrollInertia, (absDistance[1] * 4) + o.scrollInertia];
    var md = parseInt(o.contentTouchScroll) || 0; /* absolute minimum distance required */
    amount[0] = absDistance[0] > md ? amount[0] : 0;
    amount[1] = absDistance[1] > md ? amount[1] : 0;
    if (d.overflowed[0]) { _drag(amount[0], durB[0], easing, "y", overwrite, false); }
    if (d.overflowed[1]) { _drag(amount[1], durB[1], easing, "x", overwrite, false); }
  }
  const _m = (ds: number, s: number) => {
    var r = [s * 1.5, s * 2, s / 1.5, s / 2];
    if (ds > 90) {
      return s > 4 ? r[0] : r[3];
    } else if (ds > 60) {
      return s > 3 ? r[3] : r[2];
    } else if (ds > 30) {
      return s > 8 ? r[1] : s > 6 ? r[0] : s > 4 ? s : r[2];
    } else {
      return s > 8 ? s : r[3];
    }
  }
  const _drag = (amount, dur, easing: string, dir: string, overwrite: string, drag: boolean) => {
    if (!amount) { return; }
    _scrollTo($this, amount.toString(), { dur: dur, scrollEasing: easing, dir, overwrite, drag });
  }
}
/* -------------------- */
