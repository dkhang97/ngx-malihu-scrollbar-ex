import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions, $event, $offset } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _createEventQuery, _stop } from '../functions/utils';
import { classMap, pluginPfx, updateTouchActive } from '../variables';

/*
DRAGGER RAIL CLICK EVENT
scrolls content via dragger rail
*/
export function _draggerRail($this: JQueryFnsWrapper) {
  var d = $this.extendUtils($data).data(pluginPfx),
    mCSB_container = $this.selectNew(`#mCSB_${d.idx}_container`),
    wrapper = mCSB_container.parent().extendUtils($dimensions, $css),
    mCSB_draggerContainer = $this.selectNew(`.mCSB_${d.idx}_scrollbar .${classMap.dragger.container}`).extendUtils($event),
    clickable;

  const eventQuery = _createEventQuery(d.idx);

  mCSB_draggerContainer.bind(eventQuery('mousedown', 'touchstart', 'pointerdown', 'MSPointerDown'), function (e) {
    updateTouchActive(true);
    if (!wrapper.selectNew(e.target as any).hasClass("mCSB_dragger")) { clickable = 1; }
  });
  mCSB_draggerContainer.bind(eventQuery('touchend', 'pointerup', 'MSPointerUp'), function () {
    updateTouchActive(false);
  });
  mCSB_draggerContainer.bind(eventQuery('click'), function (e: MouseEvent) {
    if (!clickable) { return; }
    clickable = 0;
    if (wrapper.selectNew(e.target as any).hasClass(classMap.dragger.container) || wrapper.selectNew(e.target as any).hasClass("mCSB_draggerRail")) {
      _stop($this);
      var el = wrapper.selectNew(this), mCSB_dragger = el.find(".mCSB_dragger").extendUtils($offset);
      if (el.parent(".mCSB_scrollTools_horizontal").length > 0) {
        if (!d.overflowed[1]) { return; }
        var dir = "x",
          clickDir = e.pageX > mCSB_dragger.offset().left ? -1 : 1,
          to = Math.abs(mCSB_container[0].offsetLeft) - (clickDir * (wrapper.width() * 0.9));
      } else {
        if (!d.overflowed[0]) { return; }
        var dir = "y",
          clickDir = e.pageY > mCSB_dragger.offset().top ? -1 : 1,
          to = Math.abs(mCSB_container[0].offsetTop) - (clickDir * (wrapper.height() * 0.9));
      }
      _scrollTo($this, to.toString(), { dir: dir, scrollEasing: "mcsEaseInOut" });
    }
  });
}
/* -------------------- */
