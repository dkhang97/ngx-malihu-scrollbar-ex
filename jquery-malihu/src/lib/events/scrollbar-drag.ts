import { MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $data, $dimensions, $event, $offset } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _onDragClasses } from '../functions/drags';
import { _canAccessIFrame, _iframe } from '../functions/iframe';
import { _coordinates, _createEventQuery, _mouseBtnLeft, _stop } from '../functions/utils';
import { oldIE, pluginPfx, updateTouchActive } from '../variables';

/*
SCROLLBAR DRAG EVENTS
scrolls content via scrollbar dragging
*/
export function _draggable($this: JQueryFnsWrapper) {
  const $d = $this.extendUtils($offset).extendUtils($dimensions);

  var d = $this.extendUtils($data).data(pluginPfx), o: MalihuScrollbarOptions = d.opt,
    draggerId = [`mCSB_${d.idx}_dragger_vertical`, `mCSB_${d.idx}_dragger_horizontal`],
    mCSB_container = $this.selectNew(`#mCSB_${d.idx}_container`),
    mCSB_dragger = $this.selectNew(draggerId.map(id => `#${id}`).join(',')).extendUtils($event),
    draggable: typeof $d, dragY: number, dragX: number,
    rds = o.advanced.releaseDraggableSelectors ? mCSB_dragger.add(o.advanced.releaseDraggableSelectors) : mCSB_dragger,
    eds = o.advanced.extraDraggableSelectors
      ? $this.selectNew(<any>!_canAccessIFrame() || top.document).add(o.advanced.extraDraggableSelectors)
      : $this.selectNew(<any>!_canAccessIFrame() || top.document);

  const eventQuery = _createEventQuery(d.idx);

  mCSB_dragger.bind(eventQuery('contextmenu'), function (e) {
    e.preventDefault(); //prevent right click
  });
  mCSB_dragger.bind<MouseEvent>(eventQuery('mousedown', 'touchstart', 'pointerdown', 'MSPointerDown'), function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    if (!_mouseBtnLeft(e)) { return; } /* left mouse button only */
    updateTouchActive(true);
    if (oldIE) { document['onselectstart'] = function () { return false; } } /* disable text selection for IE < 9 */
    _iframe(mCSB_container, false); /* enable scrollbar dragging over iframes by disabling their events */
    _stop($this);
    draggable = $d.selectNew(this);
    var offset = draggable.offset(), y = _coordinates($this.document, e)[0] - offset.top, x = _coordinates($this.document, e)[1] - offset.left,
      h = draggable.height() + offset.top, w = draggable.width() + offset.left;
    if (y < h && y > 0 && x < w && x > 0) {
      dragY = y;
      dragX = x;
    }
    _onDragClasses(draggable, "active", o.autoExpandScrollbar);
  });
  mCSB_dragger.bind<TouchEvent>(eventQuery('touchmove'), function (e) {
    e.stopImmediatePropagation();
    e.preventDefault();
    var offset = draggable.offset(), y = _coordinates($this.document, e)[0] - offset.top, x = _coordinates($this.document, e)[1] - offset.left;
    _drag(dragY, dragX, y, x);
  });
  const docWithEds = $this.selectDocument().add(eds).extendUtils($event);
  docWithEds.bind<MouseEvent>(eventQuery('mousemove', 'pointermove', 'MSPointerMove'), function (e) {
    if (draggable) {
      var offset = draggable.offset(), y = _coordinates($this.document, e)[0] - offset.top, x = _coordinates($this.document, e)[1] - offset.left;
      if (dragY === y && dragX === x) { return; } /* has it really moved? */
      _drag(dragY, dragX, y, x);
    }
  });
  docWithEds.add(rds).bind(eventQuery('mouseup', 'touchend', 'pointerup', 'MSPointerUp'), function () {
    if (draggable) {
      _onDragClasses(draggable, "active", o.autoExpandScrollbar);
      draggable = null;
    }
    updateTouchActive(false);
    if (oldIE) { document['onselectstart'] = null; } /* enable text selection for IE < 9 */
    _iframe(mCSB_container, true); /* enable iframes events */
  });
  const _drag = (dragY: number, dragX: number, y: number, x: number) => {
    mCSB_container[0]['idleTimer'] = o.scrollInertia < 233 ? 250 : 0;
    if (draggable[0].getAttribute("id") === draggerId[1]) {
      var dir = "x", to = ((draggable[0].offsetLeft - dragX) + x) * d.scrollRatio.x;
    } else {
      var dir = "y", to = ((draggable[0].offsetTop - dragY) + y) * d.scrollRatio.y;
    }
    _scrollTo($this, to.toString(), { dir: dir, drag: true });
  }
}
/* -------------------- */




