import { MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $data, $dimensions, $event, $pseudo } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _childPos, _stop } from '../functions/utils';
import { pluginPfx } from '../variables';

const focusTimerMap = new WeakMap<HTMLElement, any>();

/*
FOCUS EVENT
scrolls content via element focus (e.g. clicking an input, pressing TAB key etc.)
*/
export function _focus($this: JQueryFnsWrapper) {
  const $w = $create($this.document, $dimensions, $event, $pseudo);

  const d = $this.extendUtils($data).data(pluginPfx), o: MalihuScrollbarOptions = d.opt,
    mCSB_container = $w(`#mCSB_${d.idx}_container`),
    wrapper = mCSB_container.parent();
  mCSB_container.bind(`focusin.${pluginPfx}_${d.idx}`, () => {
    const el = $w($this.document.activeElement as any),
      nested = mCSB_container.find(".mCustomScrollBox").length,
      dur = 0;

    const scrollOnFocus = o.advanced.autoScrollOnFocus;

    if (scrollOnFocus !== true) {
      if (!scrollOnFocus) {
        return;
      }

      if (typeof scrollOnFocus === 'function') {
        if (!scrollOnFocus.call($this, document.activeElement as any, mCSB_container[0])) {
          return;
        }
      } else if (!el.is(scrollOnFocus as any)) {
        return;
      }
    }

    _stop($this);
    _clearFocus($this[0]);

    focusTimerMap.set($this[0], setTimeout(function () {
      var to = [_childPos(el)[0], _childPos(el)[1]],
        contentPos = [mCSB_container[0].offsetTop, mCSB_container[0].offsetLeft],
        isVisible = [
          (contentPos[0] + to[0] >= 0 && contentPos[0] + to[0] < wrapper.height() - el.outerHeight(false)),
          (contentPos[1] + to[1] >= 0 && contentPos[0] + to[1] < wrapper.width() - el.outerWidth(false))
        ],
        overwrite = (o.axis === "yx" && !isVisible[0] && !isVisible[1]) ? "none" : "all";
      if (o.axis !== "x" && !isVisible[0]) {
        _scrollTo($this, to[0].toString(), { dir: "y", scrollEasing: "mcsEaseInOut", overwrite: overwrite, dur: dur });
      }
      if (o.axis !== "y" && !isVisible[1]) {
        _scrollTo($this, to[1].toString(), { dir: "x", scrollEasing: "mcsEaseInOut", overwrite: overwrite, dur: dur });
      }
    }, nested ? (dur + 17) * nested : 0));
  });
}
/* -------------------- */

export function _clearFocus(elem: HTMLElement) {
  if (elem) {
    const handler = focusTimerMap.get(elem);
    if (handler !== undefined) {
      clearTimeout(handler);
      focusTimerMap.delete(elem);
    }
  }
}
