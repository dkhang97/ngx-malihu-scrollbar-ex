import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $data, $dimensions, $event, $pseudo } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _canAccessIFrame } from '../functions/iframe';
import { _sequentialScroll } from '../functions/sequential-scroll';
import { _createEventQuery, _stop } from '../functions/utils';
import { pluginPfx } from '../variables';

/*
KEYBOARD EVENTS
scrolls content via keyboard
Keys: up arrow, down arrow, left arrow, right arrow, PgUp, PgDn, Home, End
*/
export function _keyboard($this: JQueryFnsWrapper) {
  const $w = $create($this.document, $event, $pseudo);

  var d = $this.extendUtils($data).data(pluginPfx), o = d.opt, seq = d.sequential,
    mCustomScrollBox = $w(`#mCSB_${d.idx}`),
    mCSB_container = $w(`#mCSB_${d.idx}_container`).extendUtils($dimensions),
    wrapper = mCSB_container.parent(),
    editables = "input,textarea,select,datalist,keygen,[contenteditable='true']",
    iframe = mCSB_container.find("iframe"),
    events = [_createEventQuery(d.idx)('blur', 'keydown', 'keyup')];
  if (iframe.length) {
    iframe.each(function () {
      $this.extendUtils($event).bind("load", function () {
        /* bind events on accessible iframes */
        if (_canAccessIFrame(<HTMLIFrameElement>this)) {
          $w(((<HTMLIFrameElement>this).contentDocument || (<HTMLIFrameElement>this).contentWindow.document) as any)
            .bind(events[0], function (e) {
              _onKeyboard(<any>e);
            });
        }
      });
    });
  }
  mCustomScrollBox[0]?.setAttribute("tabindex", "0");
  mCustomScrollBox.bind<KeyboardEvent>(events[0], function (e) {
    _onKeyboard(e.originalEvent);
  });
  const _onKeyboard = (e: KeyboardEvent) => {

    const _seq = (a: string, c) => {
      seq.type = o.keyboard.scrollType;
      seq.scrollAmount = o.keyboard.scrollAmount;
      if (seq.type === "stepped" && d.tweenRunning) { return; }
      _sequentialScroll($this, a, c);
    }

    switch (e.type) {
      case "blur":
        if (d.tweenRunning && seq.dir) { _seq("off", null); }
        break;
      case "keydown": case "keyup":
        var code = e.keyCode ? e.keyCode : e.which, action = "on";
        if ((o.axis !== "x" && (code === 38 || code === 40)) || (o.axis !== "y" && (code === 37 || code === 39))) {
          /* up (38), down (40), left (37), right (39) arrows */
          if (((code === 38 || code === 40) && !d.overflowed[0]) || ((code === 37 || code === 39) && !d.overflowed[1])) { return; }
          if (e.type === "keyup") { action = "off"; }
          if (!$w($this.document.activeElement).is(editables)) {
            e.preventDefault();
            e.stopImmediatePropagation();
            _seq(action, code);
          }
        } else if (code === 33 || code === 34) {
          /* PgUp (33), PgDn (34) */
          if (d.overflowed[0] || d.overflowed[1]) {
            e.preventDefault();
            e.stopImmediatePropagation();
          }
          if (e.type === "keyup") {
            _stop($this);
            var keyboardDir = code === 34 ? -1 : 1;
            if (o.axis === "x" || (o.axis === "yx" && d.overflowed[1] && !d.overflowed[0])) {
              var dir = "x", to = Math.abs(mCSB_container[0].offsetLeft) - (keyboardDir * (wrapper.width() * 0.9));
            } else {
              var dir = "y", to = Math.abs(mCSB_container[0].offsetTop) - (keyboardDir * (wrapper.height() * 0.9));
            }
            _scrollTo($this, to.toString(), { dir: dir, scrollEasing: "mcsEaseInOut" });
          }
        } else if (code === 35 || code === 36) {
          /* End (35), Home (36) */
          if (!$w($this.document.activeElement).is(editables)) {
            if (d.overflowed[0] || d.overflowed[1]) {
              e.preventDefault();
              e.stopImmediatePropagation();
            }
            if (e.type === "keyup") {
              if (o.axis === "x" || (o.axis === "yx" && d.overflowed[1] && !d.overflowed[0])) {
                var dir = "x", to = code === 35 ? Math.abs(wrapper.width() - mCSB_container.outerWidth(false)) : 0;
              } else {
                var dir = "y", to = code === 35 ? Math.abs(wrapper.height() - mCSB_container.outerHeight(false)) : 0;
              }
              _scrollTo($this, to.toString(), { dir: dir, scrollEasing: "mcsEaseInOut" });
            }
          }
        }
        break;
    }
  }
}
/* -------------------- */
