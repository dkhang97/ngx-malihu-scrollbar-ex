import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $$mouseWheel } from 'ngx-malihu-scrollbar-ex/jquery-fns/events';
import { $data, $dimensions, $event } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _canAccessIFrame } from '../functions/iframe';
import { _createEventQuery, _disableMousewheel, _stop } from '../functions/utils';
import { oldIE, pluginPfx } from '../variables';

/*
MOUSE WHEEL EVENT
scrolls content via mouse-wheel
via mouse-wheel plugin (https://github.com/brandonaaron/jquery-mousewheel)
*/
export function _mousewheel($this: JQueryFnsWrapper) {
  const $w = $create($this.document, $dimensions, $event);

  var d = $this.extendUtils($data).data(pluginPfx);
  if (!d) { return; } /* Check if the scrollbar is ready to use mousewheel events (issue: #185) */

  var o = d.opt,
    mCustomScrollBox = $w(`#mCSB_${d.idx}`),
    mCSB_dragger = [$w(`#mCSB_${d.idx}_dragger_vertical`), $w(`#mCSB_${d.idx}_dragger_horizontal`)],
    iframe = $w(`#mCSB_${d.idx}_container`).find("iframe");

  const eventQuery = _createEventQuery(d.idx);

  $$mouseWheel.register();

  if (iframe.length) {
    iframe.each(function () {
      $w(this).bind("load", function () {
        /* bind events on accessible iframes */
        if (_canAccessIFrame(<HTMLIFrameElement>this)) {
          $w((<HTMLIFrameElement>this).contentDocument || (<HTMLIFrameElement>this).contentWindow.document as any)
            .bind(eventQuery('mousewheel'), function (e, delta) {
              _onMousewheel(e, delta);
            });
        }
      });
    });
  }
  mCustomScrollBox.bind(eventQuery('mousewheel'), function (e, delta) {
    _onMousewheel(e, delta);
  });
  const _onMousewheel = (e, delta: number) => {
    _stop($this);
    if (_disableMousewheel($this, e.target)) { return; } /* disables mouse-wheel when hovering specific elements */
    var deltaFactor = o.mouseWheel.deltaFactor !== "auto" ? parseInt(o.mouseWheel.deltaFactor) : (oldIE && e.deltaFactor < 100) ? 100 : e.deltaFactor || 100,
      dur = o.scrollInertia;
    if (o.axis === "x" || o.mouseWheel.axis === "x") {
      var dir = "x",
        px = [Math.round(deltaFactor * d.scrollRatio.x), parseInt(o.mouseWheel.scrollAmount)],
        amount = o.mouseWheel.scrollAmount !== "auto" ? px[1] : px[0] >= mCustomScrollBox.width() ? mCustomScrollBox.width() * 0.9 : px[0],
        contentPos = Math.abs($w(`#mCSB_${d.idx}_container`)[0].offsetLeft),
        draggerPos = mCSB_dragger[1][0].offsetLeft,
        limit = mCSB_dragger[1].parent().width() - mCSB_dragger[1].width(),
        dlt = o.mouseWheel.axis === "y" ? (e.deltaY || delta) : e.deltaX;
    } else {
      var dir = "y",
        px = [Math.round(deltaFactor * d.scrollRatio.y), parseInt(o.mouseWheel.scrollAmount)],
        amount = o.mouseWheel.scrollAmount !== "auto" ? px[1] : px[0] >= mCustomScrollBox.height() ? mCustomScrollBox.height() * 0.9 : px[0],
        contentPos = Math.abs($w(`#mCSB_${d.idx}_container`)[0].offsetTop),
        draggerPos = mCSB_dragger[0][0].offsetTop,
        limit = mCSB_dragger[0].parent().height() - mCSB_dragger[0].height(),
        dlt = e.deltaY || delta;
    }
    if ((dir === "y" && !d.overflowed[0]) || (dir === "x" && !d.overflowed[1])) { return; }
    if (o.mouseWheel.invert || e.webkitDirectionInvertedFromDevice) { dlt = -dlt; }
    if (o.mouseWheel.normalizeDelta) { dlt = dlt < 0 ? -1 : 1; }
    if ((dlt > 0 && draggerPos !== 0) || (dlt < 0 && draggerPos !== limit) || o.mouseWheel.preventDefault) {
      e.stopImmediatePropagation();
      e.preventDefault();
    }
    if (e.deltaFactor < 5 && !o.mouseWheel.normalizeDelta) {
      //very low deltaFactor values mean some kind of delta acceleration (e.g. osx trackpad), so adjusting scrolling accordingly
      amount = e.deltaFactor; dur = 17;
    }
    _scrollTo($this, (contentPos - (dlt * amount)).toString(), { dir: dir, dur: dur });
  }
};
/* -------------------- */
