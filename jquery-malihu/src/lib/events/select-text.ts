import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $data, $dimensions, $event, $offset } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _sequentialScroll } from '../functions/sequential-scroll';
import { _coordinates, _createEventQuery } from '../functions/utils';
import { pluginPfx, touchable, updateTouchActive } from '../variables';

/*
SELECT TEXT EVENTS
scrolls content when text is selected
*/
export function _selectable($this: JQueryFnsWrapper) {
  const $thisExtra = $this.extendUtils($event, $dimensions, $offset);

  var d = $thisExtra.extendUtils($data).data(pluginPfx), o = d.opt, seq = d.sequential,
    mCSB_container = $thisExtra.selectNew(`#mCSB_${d.idx}_container`),
    wrapper = mCSB_container.parent(),
    action;

  const eventQuery = _createEventQuery(d.idx);

  mCSB_container.bind(eventQuery('mousedown'), function (e) {
    if (touchable) { return; }
    if (!action) {
      action = 1;
      updateTouchActive(true);
    }
  });
  const containerWithDoc = mCSB_container.add(<any>document);
  containerWithDoc.bind<MouseEvent>(eventQuery('mousemove'), function (e) {
    if (!touchable && action && _sel()) {
      var offset = mCSB_container.offset(),
        y = _coordinates($this.document, e)[0] - offset.top + mCSB_container[0].offsetTop, x = _coordinates($this.document, e)[1] - offset.left + mCSB_container[0].offsetLeft;
      if (y > 0 && y < wrapper.height() && x > 0 && x < wrapper.width()) {
        if (seq.step) { _seq("off", null, "stepped"); }
      } else {
        if (o.axis !== "x" && d.overflowed[0]) {
          if (y < 0) {
            _seq("on", 38);
          } else if (y > wrapper.height()) {
            _seq("on", 40);
          }
        }
        if (o.axis !== "y" && d.overflowed[1]) {
          if (x < 0) {
            _seq("on", 37);
          } else if (x > wrapper.width()) {
            _seq("on", 39);
          }
        }
      }
    }
  });
  containerWithDoc.bind(eventQuery('mouseup', 'dragend'), function () {
    if (touchable) { return; }
    if (action) { action = 0; _seq("off", null); }
    updateTouchActive(false);
  });

  const _sel = () => $thisExtra.document?.defaultView?.getSelection
    ? $thisExtra.document.defaultView.getSelection().toString() :
    ($thisExtra.document?.['selection'] ?? { type: 'Control' }).type != "Control"
      ? $thisExtra.document['selection'].createRange().text : 0;

  const _seq = (a: string, c: number, s?: string) => {
    seq.type = s && action ? "stepped" : "stepless";
    seq.scrollAmount = 10;
    _sequentialScroll($this, a, c, "mcsLinearOut", s ? 60 : null);
  }
}
/* -------------------- */
