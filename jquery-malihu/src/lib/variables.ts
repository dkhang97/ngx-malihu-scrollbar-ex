export const pluginNS = "mCustomScrollbar",
  pluginPfx = "mCS";
  // defaultSelector = ".mCustomScrollbar";

let totalInstances = 0; /* plugin instances amount */

export function generateInstance() {
  return ++totalInstances;
}

export const oldIE = (window['attachEvent'] && !window.addEventListener) ? 1 : 0; /* detect IE < 9 */

export let touchActive = false;
export function updateTouchActive(active: boolean) {
  touchActive = active;
}

export let touchable: 1 | 0; /* global touch vars (for touch and pointer events) */
export function updateTouchable(enable: boolean) {
  touchable = enable ? 1 : 0;
}


/* general plugin classes */
export const classMap = {
  dragger: {
    onDrag: 'mCSB_dragger_onDrag',
    container: 'mCSB_draggerContainer'
  },
  scrollTools: {
    onDrag: 'mCSB_scrollTools_onDrag',
  },
  img: {
    loaded: 'mCS_img_loaded'
  },
  disabled: 'mCS_disabled',
  destroyed: 'mCS_destroyed',
  noScrollBar: {
    all: 'mCS_no_scrollbar',
    x: 'mCS_no_scrollbar_x',
    y: 'mCS_no_scrollbar_y',
  },
  autoHide: 'mCS-autoHide',
  rtl: 'mCS-dir-rtl',
  hidden: {
    x: 'mCS_x_hidden',
    y: 'mCS_y_hidden',
  },
  button: {
    up: 'mCSB_buttonUp',
    down: 'mCSB_buttonDown',
    left: 'mCSB_buttonLeft',
    right: 'mCSB_buttonRight',
  }
}
