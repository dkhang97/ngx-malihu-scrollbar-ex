import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _removeAutoUpdate } from '../functions/auto-update';
import { _unbindEvents } from '../functions/event-binding';
import { _resetContentPosition } from '../functions/transition';
import { _scrollbarVisibility, _stop } from '../functions/utils';
import { classMap, pluginPfx } from '../variables';

/*
plugin stop method
stops scrolling animation
*/
export function stop(el: JQueryFnsWrapper) {
  return el.each(function () {
    const $this = el.selectNew(this).extendUtils($data);

    if ($this.data(pluginPfx)) { /* check if plugin has initialized */
      _stop($this);
    }
  });

}
/* ---------------------------------------- */



/*
plugin disable method
temporarily disables the scrollbar(s)
*/
export function disable(el: JQueryFnsWrapper, reset = false) {
  const $w = $create(el.document, $data, $css);

  return el.each(function () {

    var $this = $w(this);

    if ($this.data(pluginPfx)) { /* check if plugin has initialized */

      _removeAutoUpdate($this);

      _unbindEvents($this); /* unbind events */

      if (reset) { _resetContentPosition($this); } /* reset content position */

      _scrollbarVisibility($this, true); /* show/hide scrollbar(s) */

      $this.addClass(classMap.disabled); /* add disable class */

    }

  });

}
