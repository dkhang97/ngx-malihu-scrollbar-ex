import { ScrollToParameter, ScrollToParameterOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { mergeObject } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { $data } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _resolveScrollTo } from '../functions/resolve-scroll';
import { _arr, _isTabHidden } from '../functions/utils';
import { pluginPfx } from '../variables';

/*
plugin scrollTo method
triggers a scrolling event to a specific value
*/

export default function scrollTo(el: JQueryFnsWrapper,
  val: ScrollToParameter | (() => ScrollToParameter), options?: ScrollToParameterOptions) {

  if (false === (val ?? false)) { return; }

  const $w = $create(el.document, $data);

  return el.each(function () {

    var $this = $w(this);

    if ($this.data(pluginPfx)) { /* check if plugin has initialized */

      var d = $this.data(pluginPfx), o = d.opt,
        /* method default options */
        methodDefaults = {
          trigger: "external", /* method is by default triggered externally (e.g. from other scripts) */
          scrollInertia: o.scrollInertia, /* scrolling inertia (animation duration) */
          scrollEasing: "mcsEaseInOut", /* animation easing */
          moveDragger: false, /* move dragger instead of content */
          timeout: 60, /* scroll-to delay */
          callbacks: true, /* enable/disable callbacks */
          onStart: true,
          onUpdate: true,
          onComplete: true
        },
        methodOptions = mergeObject(methodDefaults, options),
        to = _arr($this, val), dur = methodOptions.scrollInertia > 0 && methodOptions.scrollInertia < 17 ? 17 : methodOptions.scrollInertia;

      /* translate yx values to actual scroll-to positions */
      to[0] = _resolveScrollTo($this, to[0], "y");
      to[1] = _resolveScrollTo($this, to[1], "x");

      /*
      check if scroll-to value moves the dragger instead of content.
      Only pixel values apply on dragger (e.g. 100, "100px", "-=100" etc.)
      */
      if (methodOptions.moveDragger) {
        to[0] *= d.scrollRatio.y;
        to[1] *= d.scrollRatio.x;
      }

      methodOptions.dur = _isTabHidden() ? 0 : dur; //skip animations if browser tab is hidden

      setTimeout(function () {
        /* do the scrolling */
        if (to[0] !== null && typeof to[0] !== "undefined" && o.axis !== "x" && d.overflowed[0]) { /* scroll y */
          methodOptions.dir = "y";
          methodOptions.overwrite = "all";
          _scrollTo($this, to[0].toString(), methodOptions);
        }
        if (to[1] !== null && typeof to[1] !== "undefined" && o.axis !== "y" && d.overflowed[1]) { /* scroll x */
          methodOptions.dir = "x";
          methodOptions.overwrite = "none";
          _scrollTo($this, to[1].toString(), methodOptions);
        }
      }, methodOptions.timeout);

    }

  });

};
