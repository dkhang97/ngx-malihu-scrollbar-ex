import { MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { mergeObject } from 'ngx-malihu-scrollbar-ex/jquery-fns/core/utils';
import { $css, $data } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { DEFAULT_OPTIONS } from '../default-options';
import { _findAxis, _findScrollButtonsType, _pluginMarkup, _theme } from '../functions/utils';
import { classMap, generateInstance, pluginPfx } from '../variables';
import update from './update';

/*
plugin initialization method
creates the scrollbar(s), plugin data object and options
----------------------------------------
*/
export default function init(el: JQueryFnsWrapper, options: MalihuScrollbarOptions) {
  options = mergeObject(DEFAULT_OPTIONS, options);

  const $w = $create(el.document, $data, $css);

  /* options backward compatibility (for versions < 3.0.0) and normalization */
  options.setWidth = options['set_width'] || options.setWidth;
  options.setHeight = options['set_height'] || options.setHeight;
  options.axis = (options['horizontalScroll']) ? "x" : _findAxis(options.axis);
  options.scrollInertia = options.scrollInertia > 0 && options.scrollInertia < 17 ? 17 : options.scrollInertia;
  if (typeof options.mouseWheel !== "object" && options.mouseWheel) { /* old school mouseWheel option (non-object) */
    options.mouseWheel = { enable: true, scrollAmount: "auto", axis: "y", preventDefault: false, deltaFactor: <any>"auto", normalizeDelta: false, invert: false }
  }
  options.mouseWheel.scrollAmount = !options.mouseWheelPixels ? options.mouseWheel.scrollAmount : options.mouseWheelPixels;
  options.mouseWheel.normalizeDelta = !options.advanced.normalizeMouseWheelDelta ? options.mouseWheel.normalizeDelta : options.advanced.normalizeMouseWheelDelta;
  options.scrollButtons.scrollType = _findScrollButtonsType(options.scrollButtons.scrollType);

  _theme(options); /* theme-specific options */

  /* plugin constructor */
  return el.each(function () {

    var $this = $w(this);

    if (!$this.data(pluginPfx)) { /* prevent multiple instantiations */

      /* store options and create objects in jquery data */
      $this.data(pluginPfx, {
        idx: generateInstance(), /* instance index */
        opt: options, /* options */
        scrollRatio: { y: null, x: null }, /* scrollbar to content ratio */
        overflowed: null, /* overflowed axis */
        contentReset: { y: null, x: null }, /* object to check when content resets */
        bindEvents: false, /* object to check if events are bound */
        tweenRunning: false, /* object to check if tween is running */
        sequential: {}, /* sequential scrolling object */
        langDir: $this.css("direction"), /* detect/store direction (ltr or rtl) */
        cbOffsets: null, /* object to check whether callback offsets always trigger */
        /*
        object to check how scrolling events where last triggered
        "internal" (default - triggered by this script), "external" (triggered by other scripts, e.g. via scrollTo method)
        usage: object.data("mCS").trigger
        */
        trigger: null,
        /*
        object to check for changes in elements in order to call the update method automatically
        */
        poll: { size: { o: 0, n: 0 }, img: { o: 0, n: 0 }, change: { o: 0, n: 0 } }
      });

      var d = $this.data(pluginPfx), o: MalihuScrollbarOptions = d.opt,
        /* HTML data attributes */
        htmlDataAxis = $this.data("mcs-axis"),
        htmlDataSbPos = $this.data("mcs-scrollbar-position"),
        htmlDataTheme = $this.data("mcs-theme");

      if (htmlDataAxis) { o.axis = htmlDataAxis; } /* usage example: data-mcs-axis="y" */
      if (htmlDataSbPos) { o.scrollbarPosition = htmlDataSbPos; } /* usage example: data-mcs-scrollbar-position="outside" */
      if (htmlDataTheme) { /* usage example: data-mcs-theme="minimal" */
        o.theme = htmlDataTheme;
        _theme(o); /* theme-specific options */
      }

      _pluginMarkup($this); /* add plugin markup */

      if (d && o.callbacks.onCreate && typeof o.callbacks.onCreate === "function") { o.callbacks.onCreate.call(this); } /* callbacks: onCreate */

      $w(`#mCSB_${d.idx}_container img:not(.${classMap.img.loaded})`).addClass(classMap.img.loaded); /* flag loaded images */

      update($this); /* call the update method */

    }
  });
}
