import { MalihuScrollbarOptions } from 'ngx-malihu-scrollbar-ex/declarations';
import { $create, JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data, $dimensions } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _scrollTo } from '../functions/animation';
import { _autoUpdate, MalihuUpdateCallback } from '../functions/auto-update';
import { _scrollRatio, _setDraggerLength } from '../functions/drags';
import { _bindEvents, _unbindEvents } from '../functions/event-binding';
import { _resetContentPosition } from '../functions/transition';
import { _contentWidth, _expandContentHorizontally, _overflowed, _scrollbarVisibility, _stop } from '../functions/utils';
import { classMap, pluginPfx } from '../variables';

/*
plugin update method
updates content and scrollbar(s) values, events and status
*/

export default function update(el: JQueryFnsWrapper, cb?: MalihuUpdateCallback) {

  const $w = $create(el.document, $data, $css, $dimensions);

  return el.each(function () {

    var $this = $w(this);

    if ($this.data(pluginPfx)) { /* check if plugin has initialized */

      var d = $this.data(pluginPfx), o: MalihuScrollbarOptions = d?.opt,
        mCSB_container = $w(`#mCSB_${d.idx}_container`),
        mCustomScrollBox = $w(`#mCSB_${d.idx}`),
        mCSB_dragger = [$w(`#mCSB_${d.idx}_dragger_vertical`), $w(`#mCSB_${d.idx}_dragger_horizontal`)];

      if (!mCSB_container.element) { return; }

      if (d.tweenRunning) { _stop($this); } /* stop any running tweens while updating */

      if (cb && typeof o?.callbacks?.onBeforeUpdate === "function") { o.callbacks.onBeforeUpdate.call(this); } /* callbacks: onBeforeUpdate */

      /* if element was disabled or destroyed, remove class(es) */
      if ($this.hasClass(classMap.disabled)) { $this.removeClass(classMap.disabled); }
      if ($this.hasClass(classMap.destroyed)) { $this.removeClass(classMap.destroyed); }

      /* css flexbox fix, detect/set max-height */
      mCustomScrollBox.css("max-height", "none");
      if (mCustomScrollBox.height() !== $this.height()) { mCustomScrollBox.css("max-height", $this.height()); }

      _expandContentHorizontally($this); /* expand content horizontally */

      if (o.axis !== "y" && !o.advanced.autoExpandHorizontalScroll) {
        mCSB_container.css("width", _contentWidth(mCSB_container));
      }

      d.overflowed = _overflowed($this); /* determine if scrolling is required */

      _scrollbarVisibility($this); /* show/hide scrollbar(s) */

      /* auto-adjust scrollbar dragger length analogous to content */
      if (o.autoDraggerLength) { _setDraggerLength($this); }

      _scrollRatio($this); /* calculate and store scrollbar to content ratio */

      _bindEvents($this); /* bind scrollbar events */

      /* reset scrolling position and/or events */
      var to = [Math.abs(mCSB_container[0].offsetTop), Math.abs(mCSB_container[0].offsetLeft)];
      if (o.axis !== "x") { /* y/yx axis */
        if (!d.overflowed[0]) { /* y scrolling is not required */
          _resetContentPosition($this); /* reset content position */
          if (o.axis === "y") {
            _unbindEvents($this);
          } else if (o.axis === "yx" && d.overflowed[1]) {
            _scrollTo($this, to[1].toString(), { dir: "x", dur: 0, overwrite: "none" });
          }
        } else if (mCSB_dragger[0].height() > mCSB_dragger[0].parent().height()) {
          _resetContentPosition($this); /* reset content position */
        } else { /* y scrolling is required */
          _scrollTo($this, to[0].toString(), { dir: "y", dur: 0, overwrite: "none" });
          d.contentReset.y = null;
        }
      }
      if (o.axis !== "y") { /* x/yx axis */
        if (!d.overflowed[1]) { /* x scrolling is not required */
          _resetContentPosition($this); /* reset content position */
          if (o.axis === "x") {
            _unbindEvents($this);
          } else if (o.axis === "yx" && d.overflowed[0]) {
            _scrollTo($this, to[0].toString(), { dir: "y", dur: 0, overwrite: "none" });
          }
        } else if (mCSB_dragger[1].width() > mCSB_dragger[1].parent().width()) {
          _resetContentPosition($this); /* reset content position */
        } else { /* x scrolling is required */
          _scrollTo($this, to[1].toString(), { dir: "x", dur: 0, overwrite: "none" });
          d.contentReset.x = null;
        }
      }

      /* callbacks: onImageLoad, onSelectorChange, onUpdate */
      if (cb) {
        if (cb === MalihuUpdateCallback.IMAGE_LOAD
          && typeof o?.callbacks?.onImageLoad === "function") {
          o.callbacks.onImageLoad.call(this);
        } else if (cb === MalihuUpdateCallback.SELECTOR_CHANGE
          && typeof o?.callbacks?.onSelectorChange === "function") {
          o.callbacks.onSelectorChange.call(this);
        } else if (typeof o?.callbacks?.onUpdate === "function") {
          o.callbacks.onUpdate.call(this);
        }
      }

      _autoUpdate($this, cb => update($this, cb));  /* initialize automatic updating (for dynamic content, fluid layouts etc.) */
    }

  });

}
