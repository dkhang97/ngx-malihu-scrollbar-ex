import { JQueryFnsWrapper } from 'ngx-malihu-scrollbar-ex/jquery-fns';
import { $css, $data } from 'ngx-malihu-scrollbar-ex/jquery-fns/extenders';

import { _removeAutoUpdate } from '../functions/auto-update';
import { _unbindEvents } from '../functions/event-binding';
import { _resetContentPosition } from '../functions/transition';
import { _delete } from '../functions/utils';
import { classMap, pluginNS, pluginPfx } from '../variables';

/*
      plugin destroy method
      completely removes the scrollbar(s) and returns the element to its original state
      */
export default function destroy(el: JQueryFnsWrapper) {
  return el.each(function () {
    var $this = el.selectNew(this).extendUtils($data, $css);

    if ($this.data(pluginPfx)) { /* check if plugin has initialized */

      var d = $this.data(pluginPfx),
        mCustomScrollBox = $this.selectNew(`#mCSB_${d.idx}`),
        mCSB_container = $this.selectNew(`#mCSB_${d.idx}_container`),
        scrollbar = el.selectNew(`.mCSB_${d.idx}_scrollbar`);

      _removeAutoUpdate($this);

      _unbindEvents($this); /* unbind events */

      _resetContentPosition($this); /* reset content position */

      $this.removeData(pluginPfx); /* remove plugin data object */

      _delete(this, "mcs"); /* delete callbacks object */

      /* remove plugin markup */
      scrollbar.remove(); /* remove scrollbar(s) first (those can be either inside or outside plugin's inner wrapper) */
      mCSB_container.find("img." + classMap.img.loaded).removeClass(classMap.img.loaded); /* remove loaded images flag */
      mCustomScrollBox.replaceWith(<any>mCSB_container.contents()); /* replace plugin's inner wrapper with the original content */
      /* remove plugin classes from the element and add destroy class */
      $this.removeClass(`${pluginNS} _${pluginPfx}_${d.idx} ${classMap.autoHide} `
        + `${classMap.rtl} ${classMap.noScrollBar.all} ${classMap.disabled}`);
      $this.addClass(classMap.destroyed);
    }
  });
}
