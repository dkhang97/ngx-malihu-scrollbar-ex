import destroy from './lib/methods/destroy';
import { disable, stop } from './lib/methods/disable';
import init from './lib/methods/init';
import scrollTo from './lib/methods/scroll-to';
import update from './lib/methods/update';

export { init, update, scrollTo, disable, stop, destroy };

export { MalihuUpdateCallback } from './lib/functions/auto-update';
